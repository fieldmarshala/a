<?php
include("inc/connect.php");
include('php/checkuser_login.php');
$db = new database();
$con = $db->connect();

//บอกว่านี่คือบทความชนิดไหน
$treatise_type = "creative";

$get_research_id = $_GET['research_id'];
$sql_res = "SELECT * FROM $treatise_type WHERE item_id = '$get_research_id'";
$query_res = $con->query($sql_res);
$result_res = $query_res->fetch_object();

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <title><?php include("inc/title.php"); ?></title>
    
    <!-- Bootstrap Core CSS -->
    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- page CSS -->
    <link href="assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/switchery/dist/switchery.min.css" rel="stylesheet" />
    <!--alerts CSS -->
    <link href="assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
    <!-- Custom CSS -->
    <link href="admin/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="admin/css/colors/blue.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> 
        </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <?php include("inc/topmenu_edituser.php") ?>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper" style="margin-left: 0px;">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <?php $page_title = "ผลงานสร้างสรรค์ของฉัน"; ?>
            <?php $page_detail = "แก้ไขข้อมูลผลงานสร้างสรรค์" ?>
            <?php $page_title_active = "breadcrumb-item";?>
            <?php $page_detail_active = "breadcrumb-item active";?>
            <?php include("inc/breadcrumb.php"); ?>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-outline-info">
                            <div class="card-header">
                                <h4 class="m-b-0 text-white"><?=$page_detail;?></h4>
                            </div>
                            <div class="card-body">
                                <div class="form-body">
                                    <h3 class="card-title"><a href="#" onclick="history.go(-1);" class="btn waves-effect waves-light btn-secondary"><i class="fa fa-lg mdi mdi-chevron-left"></i>ย้อนกลับ</a></h3>
                                    <form id="form_research" name="form_research" method="post" action="">
                                        <!--/row-->
                                        <div class="row p-t-20">
                                            <div class="col-md-12">
                                                <div id="form_creative_name" class="form-group">
                                                    <label id="label_creative_name" class="control-label">ชื่อผลงาน</label>
                                                    <!-- ส่ง treatise_id ไป -->
                                                    <input type="hidden" id="treatise_id" name="treatise_id" value="<?= $result_res->item_id; ?>">
                                                    <!-- ส่ง treatise_type = research ไป -->
                                                    <input type="hidden" id="treatise_type" name="treatise_type" value="<?= $treatise_type; ?>">
                                                    <input type="text" id="creative_name" name="creative_name" class="form-control" value="<?= $result_res->creative_name; ?>">
                                                </div>
                                                <?php
                                                $sql_user = "SELECT * FROM author WHERE treatise_type = '$treatise_type' AND treatise_id = '$get_research_id'";
                                                $query_user = $con->query($sql_user);
                                                $num_user = $query_user->num_rows;
                                                if ($num_user < 10) { ?>
                                                    <!-- เช็คว่าผู้วิจัยเท่ากับ 8 คนหรือไม่ ถ้าน้อยกว่า 8 คน แสดงกล่องเพิ่มรายชื่อผู้วิจัย -->
                                                    <label id="label_author_add" class="control-label">เพิ่มชื่อผู้แต่ง</label>
                                                    <div id="author_form" class="form-group">
                                                        <div class="entryZ input-group mb-3">
                                                            <select id="add_academic_rank" name="add_academic_rank" class="form-control col-md-12">
                                                                <option value="" selected>---- ตำแหน่งทางวิชาการ ----</option>
                                                                <option value="pro">ศ.</option>
                                                                <option value="asso">ร.ศ.</option>
                                                                <option value="assis">ผ.ศ.</option>
                                                                <option value="lec">อาจารย์</option>
                                                            </select>
                                                            <select id="add_name_title" name="add_name_title" class="form-control col-md-12">
                                                                <option value="" selected>------- คำนำหน้าชื่อ -------</option>
                                                                <option value="mr">นาย</option>
                                                                <option value="ms">นางสาว</option>
                                                                <option value="dr">ดร.</option>
                                                            </select>
                                                            <input type="text" id="add_name" class="form-control col-md-12" name="add_name" placeholder="ชื่อ">
                                                            <input type="text" id="add_surname" class="form-control col-md-12" name="add_surname" placeholder="นามสกุล">
                                                            <span class="input-group-btn col-md-2">
                                                                <button type="button" id="add_author" class="btn btn-primary col-md-12">เพิ่มชื่อผู้แต่ง</button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                <?php }; ?>
                                                <div id="form_researcher" class="form-group">
                                                    <label id="label_researcher" class="control-label">รายชื่อผู้แต่ง</label>
                                                    <table id="research_table" class="table table-bordered table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th width="5%">#</th>
                                                                <th width="7%">ตำแหน่งทางวิชาการ</th>
                                                                <th width="5%">คำนำหน้าชื่อ</th>
                                                                <th width="15%">ชื่อ</th>
                                                                <th width="15%">นามสกุล</th>
                                                                <th width="1%">ลบ</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            $sql_author = "SELECT * FROM author WHERE treatise_type = '$treatise_type' AND treatise_id = '$get_research_id' ORDER BY author_id";
                                                            $query_author = $con->query($sql_author);
                                                            $i = 1;

                                                            while ($result_author = $query_author->fetch_object()) {

                                                                $name_title = $result_author->name_title;
                                                                if ($name_title == "dr") {
                                                                    $name_title_show = "ดร.";
                                                                } else if ($name_title == "mr") {
                                                                    $name_title_show = "นาย";
                                                                } else if ($name_title == "ms") {
                                                                    $name_title_show = "นางสาว";
                                                                }

                                                                $academic_rank = $result_author->academic_rank;
                                                                if ($academic_rank == "pro") {
                                                                    $academic_rank_show = "ศ.";
                                                                } else if ($academic_rank == "asso") {
                                                                    $academic_rank_show = "ร.ศ.";
                                                                } else if ($academic_rank == "assis") {
                                                                    $academic_rank_show = "ผ.ศ.";
                                                                } else if ($academic_rank == "lec") {
                                                                    $academic_rank_show = "อาจารย์";
                                                                }
                                                                ?>
                                                                <tr>
                                                                    <td><?= "คนที่ " . $i; ?></td>
                                                                    <td>
                                                                        <!-- ส่ง author_id ไป -->
                                                                        <input type="hidden" id="author_id" name="author_id<?= $i; ?>" value="<?= $result_author->author_id; ?>">
                                                                        <select id="academic_rank" name="academic_rank<?= $i; ?>" class="select2">
                                                                            <option value="<?= $result_author->academic_rank; ?>" selected><?= $academic_rank_show; ?></option>
                                                                            <option value="">---- เลือก ----</option>
                                                                            <option value="pro">ศ.</option>
                                                                            <option value="asso">ร.ศ.</option>
                                                                            <option value="assis">ผ.ศ.</option>
                                                                            <option value="lec">อาจารย์</option>
                                                                        </select>
                                                                    </td>
                                                                    <td>
                                                                        <select id="name_title" name="name_title<?= $i; ?>" class="select2">
                                                                            <option value="<?= $result_author->name_title; ?>" selected><?= $name_title_show; ?></option>
                                                                            <option value="">---- เลือก ----</option>
                                                                            <option value="mr">นาย</option>
                                                                            <option value="ms">นางสาว</option>
                                                                            <option value="dr">ดร.</option>
                                                                        </select>
                                                                    </td>
                                                                    <td><input type="text" class="form-control" name="name<?= $i; ?>" value=<?= $result_author->name; ?>></td>
                                                                    <td><input type="text" class="form-control" name="surname<?= $i; ?>" value=<?= $result_author->surname; ?>></td>
                                                                    <td align=center><button id="delete" type="button" class="btn waves-effect waves-light btn-danger" onclick="return delete_author('<?php echo $result_author->author_id; ?>')"><i class="fa fa-trash-o fa-lg"></i></button></td>
                                                                </tr>
                                                                <?php $i++;
                                                            } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div id="form_year_make" class="form-group">
                                                    <label id="label_year_make" class="control-label">ปีที่แต่ง (พ.ศ.)</label>
                                                    <input type="text" id="year_make" name="year_make" data-date-language="th-th" class="form-control" value="<?= $result_res->year_make; ?>">
                                                </div>
                                                <div id="form_reward" class="form-group">
                                                    <label id="label_reward" class="control-label">ได้รับรางวัล</label>
                                                    <input type="text" id="reward" name="reward" class="form-control" value="<?= $result_res->reward; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/row-->
                                    </form>
                                </div> <!-- form body -->
                                <div class="form-actions">
                                    <button type="button" id="edit_res" class="btn btn-success col-md-1">บันทึก</button>
                                    <button onclick="history.go(-1);" class="btn btn-inverse col-md-1">ยกเลิก</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row -->               
                <!-- ============================================================== -->
                <!-- End PAge Content -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/plugins/bootstrap/js/popper.min.js"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="admin/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="admin/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="admin/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="admin/js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <script src="assets/plugins/switchery/dist/switchery.min.js"></script>
    <script src="assets/plugins/select2/dist/js/select2.full.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
    <script src="assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
    <script src="assets/plugins/multiselect/js/jquery.multi-select.js" type="text/javascript"></script>
    <!-- Sweet-Alert  -->
    <script src="assets/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="assets/plugins/sweetalert/jquery.sweet-alert.custom.js"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- JS Datepicker TH -->
    <script src="js/bootstrap-datepicker/js/bootstrap-datepicker-thai.js"></script>
    <script src="js/bootstrap-datepicker/locales/bootstrap-datepicker.th.js"></script>
    <!-- custom js -->
    <script src="js/user.js"></script>  <!-- สำหรับเพิ่มรายชื่อผู้วิจัย -->
    <script src="js/creative.js"></script>
    <script src="js/login.js"></script>
    <script>
        $(document).ready(function() {
            $(".select2").select2({
                minimumResultsForSearch: -1 // ปิด Textbox ของ select2 ตอนเลือก listbox
            });
        });
    </script>
    <script>
        var num_author = '<?php echo $num_user; ?>';
        if (num_author == 1) {
            $("#delete").attr('disabled', true);
        } else {
            $("#delete").attr('disabled', false);
        }
    </script>
</body>

</html>