<?php
error_reporting(E_ALL ^ E_NOTICE);
session_start();
include("inc/connect.php");
include("php/checkuser_login.php");
$db = new database();
$con = $db->connect();

?>
<!DOCTYPE html>
<html lang="th">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <title><?php include("inc/title.php"); ?></title>

  <!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
  <link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css" media="screen" />

  <!-- Bootstrap Core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom CSS -->
  <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="css/ionicons.min.css" rel="stylesheet">
  <link href="css/main.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
  <link href="css/responsive.css" rel="stylesheet">
  <link href="font/flaticon.css" rel="stylesheet">
  <!-- Bootstrap Core CSS -->
  <link href="assets/plugins/bootstrap-3.4.1/css/bootstrap.css" rel="stylesheet" >
  <!-- JavaScripts -->
  <script src="js/modernizr.js"></script>

  <!-- Online Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Merriweather:300,400,700,900|Poppins:300,400,500,600,700|Montserrat:300,400,500,600,700,800" rel="stylesheet">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
  <style type="text/css">
    .fa {
      font-size: 40px;
      margin-bottom: 10px;
    }
  </style>

</head>

<body>

  <!-- LOADER -->
  <!-- <div id="loader">
  <div class="position-center-center">
    <div class="ldr"></div>
  </div>
</div> -->

  <!-- Wrap -->
  <div id="wrap">
    <!-- TOP Bar -->
    <div class="top-bar">
      <div class="container-full">
        <!-- Login Info -->
        <?php include("inc/login_info.php"); ?>
      </div>
    </div>
    <!--======= SUB BANNER =========-->
    <?php include("inc/subbanner.php"); ?>
    <!-- Content -->
    <div id="content">
      <!-- Products -->
      <section class="shop-page padding-top-50 padding-bottom-150">
        <div class="container-full">
          <div class="row">
            <!-- Item Content -->
            <div class="col-md-12">
              <div class="sidebar-layout">
                <!-- Item -->
                <div id="products" class="arrival-block col-item-4 list-group">
                  <h4>
                    <div class="row p-t-10">
                      <div class="col-md-4">
                        <div align="center">
                          <a href="user_edit.php?user_id=<?= $fashionuser_id; ?>"><i class="fa fa-user " aria-hidden="true"></i><br>ข้อมูลส่วนตัว</a>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div align="center">
                          <a href="report.php"><i class="fa fa-clipboard" aria-hidden="true"></i><br>ผลงานทั้งหมด</a>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div align="center">
                          <ul style="list-style-type: none;">
                            <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-plus-circle"></i><br>เพิ่มผลงาน/งานวิจัย<b class="caret"></b></a>
                              <ul class="dropdown-menu">
                                <li><a href="research_add.php">เพิ่มข้อมูลงานวิจัย</a></li>
                                <li><a href="book_add.php">เพิ่มข้อมูลหนังสือ</a></li>
                                <li><a href="textbook_add.php">เพิ่มข้อมูลตำรา</a></li>
                                <li><a href="research_article_add.php">เพิ่มข้อมูลบทความวิจัย</a></li>
                                <li><a href="academic_article_add.php">เพิ่มข้อมูลบทความวิชาการ</a></li>
                                <li><a href="teaching_add.php">เพิ่มข้อมูลเอกสารประกอบการสอน</a></li>
                                <li><a href="creative_add.php">เพิ่มข้อมูลผลงานสร้างสรรค์</a></li>
                                <li><a href="conference_add.php">เพิ่มข้อมูลการนำเสนอในที่ประชุม</a></li>
                              </ul>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </h4><br><br>

                </div> <!-- products -->
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>

    <!-- GO TO TOP  -->
    <a href="#" class="cd-top"><i class="fa fa-angle-up"></i></a>
    <!-- GO TO TOP End -->

  </div>
  <script src="js/jquery-1.12.4.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/own-menu.js"></script>
  <script src="js/jquery.lighter.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/main.js"></script>
</body>

</html>