<?php

include("inc/connect.php");
include('php/checkuser_login.php');
require 'assets/plugins/phpspreadsheet/vendor/autoload.php';
$db = new database();
$con = $db->connect();
error_reporting(0);

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

$treatise_type = "creative";
$search_name = $_GET['search_name'];
$search_surname = $_GET['search_surname'];
$start_year = $_GET['start_year'];
$end_year = $_GET['end_year'];

if ($search_name) {
    $sql_creative = "SELECT author.*,creative.* ";
    $sql_creative .= "FROM author,creative ";
    $sql_creative .= "WHERE author.treatise_id = creative.item_id ";
    $sql_creative .= "and author.name = '$search_name' ";
    $sql_creative .= "and author.surname = '$search_surname' ";
    $sql_creative .= "and treatise_type = 'creative' ";
    if ($start_year) {
        $sql_creative .= "and year_make >= '$start_year' ";
    } else if ($end_year) {
        $sql_creative .= "and year_make <= '$end_year' ";
    } else if ($start_year && $end_year) {
        $sql_creative .= "and year_make >= '$start_year' and year_make <= '$end_year' ";
    }
} else {
    $sql_creative = "SELECT * FROM creative ";
    if ($start_year) {
        $sql_creative .= "WHERE year_make >= '$start_year' ";
    } else if ($end_year) {
        $sql_creative .= "WHERE year_make <= '$end_year' ";
    } else if ($start_year && $end_year) {
        $sql_creative .= "WHERE year_make >= '$start_year' and year_make <= '$end_year' ";
    }
}

$sql_creative .= "ORDER BY creative_name";

$query_creative = $con->query($sql_creative);

$spreadsheet = new Spreadsheet();
$spreadsheet->getDefaultStyle()->getFont()->setName('TH Sarabun New');
$spreadsheet->getDefaultStyle()->getFont()->setSize(18);
$spreadsheet->getDefaultStyle()->getAlignment()->setHorizontal('center');
$spreadsheet->getActiveSheet()->getStyle('A1:E1')->getFont()->setSize(22);
$spreadsheet->getActiveSheet()->getStyle('A1:E1')->getFont()->setBold('Bold');
$spreadsheet->getActiveSheet()->getStyle('E')->getAlignment()->setHorizontal('left');
for ($col = 'A'; $col != 'F'; $col++) {
    $spreadsheet->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
}
$sheet = $spreadsheet->getActiveSheet();
$sheet->setCellValue('A1', 'เรื่องที่');
$sheet->setCellValue('B1', 'ชื่อผลงานสร้างสรรค์');
$sheet->setCellValue('C1', 'ปีที่ผลิต');
$sheet->setCellValue('D1', 'ได้รับรางวัล');
$sheet->setCellValue('E1', 'รายชื่อผู้แต่ง');

if ($query_creative->num_rows > 0) {
    $result_row = 1;
    $i = 1;
    while ($result_creative = $query_creative->fetch_object()) {

        $item_id = $result_creative->item_id;
        //fetch author
        $sql_author = "SELECT * FROM author WHERE treatise_type = '$treatise_type' AND treatise_id = '$item_id' ORDER BY author_id";
        $query_author = $con->query($sql_author);
        $num_author = $query_author->num_rows;

        $creative_name = $result_creative->creative_name;
        $year_make = $result_creative->year_make;
        $reward = $result_creative->reward;

        $rowNum = $result_row + 1;

        $sheet->setCellValue('A' . $rowNum, $i);
        $sheet->setCellValue('B' . $rowNum, $creative_name);
        $sheet->setCellValue('C' . $rowNum, $year_make);
        $sheet->setCellValue('D' . $rowNum, $reward);
        $sheet->setCellValue('E' . $rowNum, "จำนวนผู้แต่ง " . $num_author . " คน");

        $ii = 1;
        while ($result_author = $query_author->fetch_object()) {
            $author_name = $result_author->name;
            $author_surname = $result_author->surname;
            $author_name_title = $result_author->name_title;
            $author_academic_rank = $result_author->academic_rank;
            if ($author_name_title == "dr") {
                $name_title_show = "ดร.";
            } else {
                $name_title_show = "";
            }

            if ($author_academic_rank == "pro") {
                $academic_rank_show = "ศ.";
            } else if ($author_academic_rank == "asso") {
                $academic_rank_show = "ร.ศ.";
            } else if ($author_academic_rank == "assis") {
                $academic_rank_show = "ผ.ศ.";
            } else if ($author_academic_rank == "lec") {
                if ($name_title_show == "ดร.") {
                    $academic_rank_show = "อาจารย์ ";
                } else {
                    $academic_rank_show = "อาจารย์ ";
                }
            }
            $author_row = $result_row + 2;
            $sheet->setCellValue('E' . $author_row, "คนที่ " . $ii . " : " . $academic_rank_show . $name_title_show . " " . $author_name . " " . $author_surname);
            $result_row++;
            $ii++;
        }
        $result_row++;
        $i++;
    }
}

$filename = 'ผลงานสร้างสรรค์-' . time() . '.xlsx';
// Redirect output to a client's web browser (Xlsx)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.

$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
