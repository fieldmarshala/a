<?php
error_reporting(E_ALL ^ E_NOTICE);
include("../inc/connect.php");
$db = new database();
$con = $db->connect();

//user แก้ไขข้อมูลส่วนตัว
if (isset($_POST['edit_user'])) {

	$user_id = $_POST['user_id'];
	$user_academic_rank = $_POST['user_academic_rank'];
	$user_name_title = $_POST['user_name_title'];
	$user_name = $_POST['user_name'];
	$user_surname = $_POST['user_surname'];
	$user_birthday = DateConvert($_POST['user_birthday']);
	$user_education = $_POST['user_education'];
	$user_tel = $_POST['user_tel'];
	$user_email = $_POST['user_email'];
	$user_local_address = $_POST['user_local_address'];
	$user_local_province = $_POST['user_local_province'];
	$user_address = $_POST['user_address'];
	$user_province = $_POST['user_province'];
	$user_id_card = $_POST['user_id_card'];
	$user_username = $_POST['user_username'];
	$user_password = $_POST['user_password'];

	$user_start_working = DateConvert($_POST['user_start_working']);
	$user_work_type = $_POST['user_work_type'];
	$user_budget_type = $_POST['user_budget_type'];
	$user_type = $_POST['user_type'];

	$old_pic = $_POST['old_pic'];
	$new_pic = $_POST['img_name'];

	$sql = "UPDATE user SET ";
	$sql .= "academic_rank = '$user_academic_rank', ";
	$sql .= "name_title = '$user_name_title', ";
	$sql .= "name = '$user_name', ";
	$sql .= "surname = '$user_surname', ";
	$sql .= "birthday = '$user_birthday', ";
	$sql .= "education = '$user_education', ";
	$sql .= "phone = '$user_tel', ";
	$sql .= "email = '$user_email', ";
	$sql .= "local_address = '$user_local_address', ";
	$sql .= "local_province = '$user_local_province', ";
	$sql .= "address = '$user_address', ";
	$sql .= "province = '$user_province', ";
	$sql .= "id_card = '$user_id_card', ";
	$sql .= "user_username = '$user_username', ";
	$sql .= "user_password = '$user_password' ";
	$sql .= "WHERE user_id = '$user_id' ";
	$con->query($sql);

	$sql_work = "UPDATE work_detail SET ";
	$sql_work .= "start_working = '$user_start_working', ";
	$sql_work .= "work_type = '$user_work_type', ";
	$sql_work .= "budget_type = '$user_budget_type', ";
	$sql_work .= "user_type = '$user_type' ";
	$sql_work .= "WHERE user_id = '$user_id' ";
	$con->query($sql_work);

	//เช็คว่ามีการอัพรูปมาใหม่หรือไม่
	if ((isset($new_pic)) && ($new_pic !== '')) {
		$sql_img = "UPDATE profile_img SET image_name = '$new_pic' WHERE user_id = '$user_id'";
		$con->query($sql_img);

		if ((isset($old_pic)) && ($old_pic !== '')) {
			$files = '../images/profiles/' . $old_pic;

			if (file_exists($files))	// image_exists คือฟังก์ชัน เช็คว่าไฟล์หรือ directory มีอยู่หรือไม่
			{
				unlink($files);
			}
		}
	}

	echo "yes";
}

//ลบรายชื่อผู้วิจัย
if (isset($_POST['delete_author'])) {
	$author_id = $_POST['author_id_2'];

	$sql = "DELETE FROM author WHERE author_id = '$author_id' ";
	$con->query($sql);

	echo "yes";
}

//เพิ่มรายชื่อผู้วิจัยหน้าจาก research_edit.php
if (isset($_POST['add_author_from_edit'])) {

	$treatise_type = $_POST['treatise_type'];
	$treatise_id = $_POST['treatise_id'];
	$name_add = $_POST['add_name'];
	if ((isset($name_add)) && ($name_add != '')) {
		$academic_rank_add = $_POST['add_academic_rank'];
		$name_title_add = $_POST['add_name_title'];
		$surname_add = $_POST['add_surname'];

		$sql_author_add = "INSERT INTO author";
		$sql_author_add .= "(name_title, academic_rank,";
		$sql_author_add .= "name, surname, treatise_type,";
		$sql_author_add .= "treatise_id)";
		$sql_author_add .= "VALUES ";
		$sql_author_add .= "('$name_title_add', '$academic_rank_add',";
		$sql_author_add .= "'$name_add', '$surname_add', '$treatise_type',";
		$sql_author_add .= "'$treatise_id')";
		$con->query($sql_author_add);
	}
	echo "yes";
}
