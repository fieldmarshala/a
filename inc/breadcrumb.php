<div class="row page-titles">
    <div class="col-md-4 align-self-center">
        <h3 class="text-themecolor"><?php echo $page_title; ?></h3>
    </div>
    <div class="col-md-6 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">หน้าแรก</a></li>
            <li class="<?= $page_title_active; ?>"><?php echo $page_title; ?></li>
            <li class="<?= $page_detail_active; ?>"><?php echo $page_detail; ?></li>
        </ol>
    </div>
    <div class="col-md-2 align-self-center">
        <li class="btn waves-effect waves-light btn-secondary col-md-12"><a href="php/login_process.php?logout=logout">ออกจากระบบ</a></li>
    </div>
</div>