<?PHP
date_default_timezone_set("Asia/Bangkok");

class database
{
	private $host = "localhost";
	//HOST DB
	//private $user = "nisakornco_science001";
	//private $password = "xRR66fub";

	private $database = "nisakornco_science001";

	//local DB
	private $user = "root";
	private $password = "";

	public function connect()
	{
		$mysqli = new mysqli($this->host, $this->user, $this->password, $this->database);
		$mysqli->set_charset("utf8");
		if ($mysqli->connect_error) {
			die("connect error " . $mysqli->connect_error);
		}
		return $mysqli;
	}

	function close()
	{
		mysqli_close($this->connect());
	}
}

// date(Y/m/d)
// 2019/06/05 --> 2562-06-05
function DateTimeNew($strDate)
{
	$strYear = date("Y", strtotime($strDate)) + 543;
	$strMonth = date("m", strtotime($strDate));
	$strDay = date("d", strtotime($strDate));
	return "$strYear-$strMonth-$strDay";
}

function GetYear($Date)	// 31/01/2562 --> 2562
{
	$datecut = explode("/", $Date);
	$dateconv = $datecut[2];
	return $dateconv;
}

//HTML Form to My SQL
function DateConvert($Date)	// 31/01/2562 --> 2562-01-31
{
	$datecut = explode("/", $Date);
	$dateconv = $datecut[2] . "-" . $datecut[1] . "-" . $datecut[0];
	return $dateconv;
}

//My SQL to HTML Form
function DateConvertBase($Date)	//  2562-01-31 --> 31/01/2562 
{
	$datecut = explode("-", $Date);
	$dateconv = $datecut[2] . "/" . $datecut[1] . "/" . $datecut[0];
	return $dateconv;
}

function DateCutTime($strDate)	//1/1/2513
{
	$strYear = date("Y", strtotime($strDate)) + 543;
	$strMonth = date("n", strtotime($strDate));
	$strDay = date("j", strtotime($strDate));
	$strHour = date("H", strtotime($strDate));
	$strMinute = date("i", strtotime($strDate));
	$strSeconds = date("s", strtotime($strDate));
	return "$strDay/$strMonth/$strYear";
}

function DateTime($strDate)	//01/01/2513 07:00:00
{
	$strYear = date("Y", strtotime($strDate)) + 543;
	$strMonth = date("m", strtotime($strDate));
	$strDay = date("d", strtotime($strDate));
	$strHour = date("H", strtotime($strDate));
	$strMinute = date("i", strtotime($strDate));
	$strSeconds = date("s", strtotime($strDate));
	return "$strDay/$strMonth/$strYear $strHour:$strMinute:$strSeconds";
}
