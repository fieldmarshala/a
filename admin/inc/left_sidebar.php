<div class="scroll-sidebar">
    <!-- User profile -->
    <div class="user-profile">
        <!-- User profile image -->
        <div class="profile-img">
            <img src="../images/profiles/<?= $image_name; ?>" alt="user" />
        </div>
        <!-- User profile text-->
        <div class="profile-text">
            <h5><?= $a_name; ?></h5>
            <span id="logout" class="" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></span>
        </div>
    </div>
    <!-- End User profile text-->
    <!-- Sidebar navigation-->
    <nav class="sidebar-nav">
        <ul id="sidebarnav">
            <li class="nav-devider"></li>
            <li> <a class="waves-effect waves-dark" href="index.php" aria-expanded="false"><i class="mdi mdi-home"></i><span class="hide-menu">หน้าแรก</span></a></li>
            <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-account"></i><span class="hide-menu">ผู้ใช้งาน</span></a>
                <ul aria-expanded="true" class="collapse">
                    <li><a href="user.php" class="mdi mdi-account-multiple"> ผู้ใช้งานทั้งหมด </a></li>
                    <li><a href="user_add.php" class="mdi mdi-account-plus"> เพิ่มผู้ใช้งาน </a></li>
                </ul>
            </li>
            <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-bullseye"></i><span class="hide-menu">ตำรา</span></a>
                <ul aria-expanded="true" class="collapse">
                    <li><a href="textbook.php">ตำราทั้งหมด</a></li>
                    <li><a href="textbook_add.php">เพิ่มตำรา</a></li>
                </ul>
            </li>
            <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-bullseye"></i><span class="hide-menu">เอกสารประกอบการสอน</span></a>
                <ul aria-expanded="true" class="collapse">
                    <li><a href="teaching.php">เอกสารประกอบการสอนทั้งหมด</a></li>
                    <li><a href="teaching_add.php">เพิ่มเอกสารประกอบการสอน</a></li>
                </ul>
            </li>
            <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-bullseye"></i><span class="hide-menu">บทความวิชาการ</span></a>
                <ul aria-expanded="true" class="collapse">
                    <li><a href="academic_article.php">บทความวิชาการทั้งหมด</a></li>
                    <li><a href="academic_article_add.php">เพิ่มบทความวิชาการ</a></li>
                </ul>
            </li>
            <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-bullseye"></i><span class="hide-menu">บทความวิจัย</span></a>
                <ul aria-expanded="true" class="collapse">
                    <li><a href="research_article.php">บทความวิจัยทั้งหมด</a></li>
                    <li><a href="research_article_add.php">เพิ่มบทความวิจัย</a></li>
                </ul>
            </li>
            <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-document"></i><span class="hide-menu">งานวิจัย</span></a>
                <ul aria-expanded="true" class="collapse">
                    <li><a href="research.php">งานวิจัยทั้งหมด</a></li>
                    <li><a href="research_add.php">เพิ่มงานวิจัย</a></li>
                </ul>
            </li>
            <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-book-open-variant"></i><span class="hide-menu">หนังสือ</span></a>
                <ul aria-expanded="true" class="collapse">
                    <li><a href="book.php">หนังสือทั้งหมด</a></li>
                    <li><a href="book_add.php">เพิ่มหนังสือ</a></li>
                </ul>
            </li>
            <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-bullseye"></i><span class="hide-menu">ผลงานสร้างสรรค์</span></a>
                <ul aria-expanded="true" class="collapse">
                    <li><a href="creative.php">ผลงานสร้างสรรค์ทั้งหมด</a></li>
                    <li><a href="creative_add.php">เพิ่มผลงานสร้างสรรค์</a></li>
                </ul>
            </li>
            <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-bullseye"></i><span class="hide-menu">การนำเสนอในที่ประชุม</span></a>
                <ul aria-expanded="true" class="collapse">
                    <li><a href="conference.php">การนำเสนอในที่ประชุมทั้งหมด</a></li>
                    <li><a href="conference_add.php">เพิ่มการนำเสนอในที่ประชุม</a></li>
                </ul>
            </li>
        </ul>
    </nav>
    <!-- End Sidebar navigation -->
</div>