jQuery.fn.extend({
    createRepeaterZ: function () {
        var addItem = function (items, key) {
            var itemContent = items;
            var group = itemContent.data("group");
            var item = itemContent;
            var input = item.find('input,select');
            
            input.each(function (index, el) {
                var attrName = $(el).data('name');
                var skipName = $(el).data('skip-name');
                if (skipName != true) {
                    $(el).attr("name", group + "[" + key + "]" + attrName);
                } else {
                    if (attrName != 'undefined') {
                        $(el).attr("name", attrName);
                    }
                }
            })
            var itemClone = items;

            $("<div class='items'>" + itemClone.html() + "<div/>").appendTo(repeater);
        };

        /* find elements */
        var repeater = this;
        var items = repeater.find(".items");
        var key = 1;
        var addButton = repeater.find('.repeater-add-btn');
        var newItem = items;

        if (key == 1) {
            items.remove();
            addItem(newItem, key);
            $("#remove-btn").attr('disabled', true);
            
        }

        /* handle click and add items */
        addButton.on("click", function () {
            key++;
            addItem(newItem, key);
             //เปิดปุ่มลบหากเหลือมากกว่า 1 คน
            $("#remove-btn").attr('disabled', false);
            //ปิดปุ่มเพิ่มเมื่อครบ 10 คน
            if (key >=10 ) {
                $(".repeater-add-btn").attr('disabled', true);
            }
        });

        $(document).on('click', '.remove-btn', function () {
            $(this).parents('.items').remove();
            key--;
            //เปิดปุ่มเพิ่มเมื่อไม่ถึง 10 คน
            if (key <=10 ) {
                $(".repeater-add-btn").attr('disabled', false);
            }
            //ปิดปุ่มลบหากเหลือคนเดียว
            if(key == 1){
                $("#remove-btn").attr('disabled', true);
            }
            return false;
        });
    }
});