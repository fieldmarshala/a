var author_count = 1;

$(function () {
    $(document).on('click', '.btn-add', function (e) {
        e.preventDefault();
        if (author_count < 8) {
            author_count++;
            var controlForm = $('.author_form:first'),
                currentEntry = $(this).parents('.entryZ:first'),
                newEntry = $(currentEntry.clone()).appendTo(controlForm);

            newEntry.find('input').val('');
            newEntry.find('input').attr("id", "name" + author_count);

            //newEntry.find('label').text('คนที่ ' + author_count); 
            // เวลาลบแถวก่อนไม่เปลี่ยน เช่น มี 3 คน คนที่ 1 คนที่ 2 คนที่ 3 เรียงลำดับลงมา ลบคนที่ 2 เหลือ คนที่ 1 กับ 3 ซึ่งคนที่ 3 ควรจะเลื่อนขึ้นมาเป็นคนที่ 2

            //controlForm.find('.entryZ:last .btn-add') ปุ่มเพิ่มจะอยู่ที่คนที่ 1 (คนแรก)   (จะลบคนแรกไม่ได้)
            //controlForm.find('.entryZ:not(:last) .btn-add') ปุ่มเพิ่มจะอยู่ที่คนล่าสุดที่เพิ่มเข้ามา  (จะลบคนล่าสุดไม่ได้)
            //controlForm.find('.entryZ:first .btn-add') คนแรกเป็นปุ่มลบ ที่เหลือเป็นเพิ่ม

            //  not(:last)  เอาทุกอัน ยกเว้นอันสุดท้าย
            controlForm.find('.entryZ:not(:last) .btn-add')
            //newEntry.find('.btn-add').attr("id", "button_add" + author_count)

            //$("#button_add" + author_count).remove()
                
                //type
                .removeClass('btn-add')
                .addClass('btn-remove') 
                
                //สี
                .removeClass('btn-success')
                .addClass('btn-danger')

                .html('<span class="mdi mdi-account-remove col-md-12"> ลบรายชื่อผู้วิจัย</span>');
        }else{
            swal({
                title: "สามารถเพิ่มรายชื่อผู้วิจัยได้สูงสุด 8 คน",
                text: "",
                type: "error",
                confirmButtonColor: "#26dad2",
                confirmButtonText: "OK"
              });
        }
    })
    .on('click', '.btn-remove', function (e) {
        $(this).parents('.entryZ:first').remove();
        author_count--;
        e.preventDefault();
        return false;
    });
});