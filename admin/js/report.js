$(function () {

    $('.datepicker').datepicker({
        format: "yyyy",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
        viewMode: "years",
        minViewMode: "years"
    }).datepicker();

    $('.DataTableTH').DataTable({
        "language": {
            "url": "js/datatables/Thai.json"
        }
    });
});

window.onscroll = function () {
    scrollFunction()
};

function scrollFunction() {
    if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}

function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}