$(function () {

  $('#year_make').datepicker({
    format: "yyyy",
    keyboardNavigation: false,
    forceParse: false,
    calendarWeeks: true,
    autoclose: true,
    viewMode: "years",
    minViewMode: "years"
  }).datepicker();

  $('#save_research').click(function () {
    var treatise_type = $('#treatise_type').val();

    var emptyName = false;
    var emptySurname = false;
    var emptyAcademic_rank = false;
    var emptyName_title = false;

    // เช็คค่าว่างของชื่อผู้แต่ง
    if ($.each($('.name'), function () {
      if ($(this).val() == '') {
        emptyName = true;
      }
    })
    );

    if ($.each($('.surname'), function () {
      if ($(this).val() == '') {
        emptySurname = true;
      }
    })
    );

    if ($.each($('.academic_rank'), function () {
      if ($(this).val() == '') {
        emptyAcademic_rank = true;
      }
    })
    );

    if ($.each($('.name_title'), function () {
      if ($(this).val() == '') {
        emptyName_title = true;
      }
    })
    );

    if (emptyName) {
      $.each($('.name'), function () {
        if ($(this).val() == '') {
          $(this).focus();
          swal({
            title: "กรุณาใส่ชื่อผู้แต่ง",
            text: "",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#26dad2",
            confirmButtonText: 'OK!',
          });
          return false;
        }
      });
    }
    else if (emptySurname) {
      $.each($('.surname'), function () {
        if ($(this).val() == '') {
          $(this).focus();
          swal({
            title: "กรุณาใส่นามสกุลผู้แต่ง",
            text: "",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#26dad2",
            confirmButtonText: 'OK!',
          });
          return false;
        }
      });
    }
    else if (emptyAcademic_rank) {
      $.each($('.academic_rank'), function () {
        if ($(this).val() == '') {
          $(this).focus();
          swal({
            title: "กรุณาเลือกตำแหน่งทางวิชาการของผู้แต่ง",
            text: "",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#26dad2",
            confirmButtonText: 'OK!',
          });
          return false;
        }
      });
    }
    else if (emptyName_title) {
      $.each($('.name_title'), function () {
        if ($(this).val() == '') {
          $(this).focus();
          swal({
            title: "กรุณาเลือกคำนำหน้าชื่อของผู้แต่ง",
            text: "",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#26dad2",
            confirmButtonText: 'OK!',
          });
          return false;
        }
      });
    }
    else if ($('#research_article_name').val() == '') {
      $('#research_article_name').focus();
      swal({
        title: "กรุณาใส่ชื่อเรื่อง",
        text: "",
        type: "error",
        showCancelButton: false,
        confirmButtonColor: "#26dad2",
        confirmButtonText: 'OK!',
      });
      return false;
    }
    else if ($('#page').val() == '') {
      $('#page').focus();
      swal({
        title: "กรุณาใส่จำนวนหน้า",
        text: "",
        type: "error",
        showCancelButton: false,
        confirmButtonColor: "#26dad2",
        confirmButtonText: 'OK!',
      });
      return false;
    }
    else if ($('#year_make').val() == '') {
      $('#year_make').focus();
      swal({
        title: "กรุณาเลือกปีที่แต่ง",
        text: "",
        type: "error",
        showCancelButton: false,
        confirmButtonColor: "#26dad2",
        confirmButtonText: 'OK!',
      });
      return false;
    }
    else if ($('#journal_name').val() == '') {
      $('#journal_name').focus();
      swal({
        title: "กรุณาใส่ชื่อวารสาร",
        text: "",
        type: "error",
        showCancelButton: false,
        confirmButtonColor: "#26dad2",
        confirmButtonText: 'OK!',
      });
      return false;
    }
    else if ($('#year').val() == '') {
      $('#year').focus();
      swal({
        title: "กรุณาใส่ปี",
        text: "",
        type: "error",
        showCancelButton: false,
        confirmButtonColor: "#26dad2",
        confirmButtonText: 'OK!',
      });
      return false;
    }
    else if ($('#copy_no').val() == '') {
      $('#copy_no').focus();
      swal({
        title: "กรุณาใส่ฉบับที่...",
        text: "",
        type: "error",
        showCancelButton: false,
        confirmButtonColor: "#26dad2",
        confirmButtonText: 'OK!',
      });
      return false;
    }
    else if ($('#start_month').val() == '') {
      $('#start_month').focus();
      swal({
        title: "กรุณาเลือกจากเดือน",
        text: "",
        type: "error",
        showCancelButton: false,
        confirmButtonColor: "#26dad2",
        confirmButtonText: 'OK!',
      });
      return false;
    }
    else if ($('#end_month').val() == '') {
      $('#end_month').focus();
      swal({
        title: "กรุณาเลือกถึงเดือน",
        text: "",
        type: "error",
        showCancelButton: false,
        confirmButtonColor: "#26dad2",
        confirmButtonText: 'OK!',
      });
      return false;
    }
    else {
      swal({
        title: "คุณต้องการบันทึกข้อมูล?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#26dad2",
        confirmButtonText: "บันทึก",
        cancelButtonText: "ยกเลิก",
        closeOnConfirm: false,
        closeOnCancel: true
      }, function (isConfirm) {
        if (isConfirm) {
          var form_research = $("#form_research");
          $.ajax({
            url: "php/research_process.php",
            type: "post", //กำหนดให้มีรูปแบบเป็น post
            data: form_research.serialize() + "&add_research=add_research",
            success: function (data) {
              if (data == "yes") {
                swal({
                  title: "บันทึกข้อมูลเรียบร้อย",
                  text: "",
                  type: "success",
                  confirmButtonColor: "#26dad2",
                  confirmButtonText: "OK"
                }, function () {
                  window.location.replace(treatise_type + ".php");
                }); // swal
              }
              else {
                console.log(data);
              }
            } // success data
          }); // ajax
        }// isConfirm
        else {
          return false;
        }
      });
    }
  }); // #save_research button

  $('#edit_res').click(function () {
    var treatise_id = $('#treatise_id').val();
    var treatise_type = $('#treatise_type').val();
    swal({
      title: "คุณต้องการบันทึกข้อมูล?",
      text: "",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#26dad2",
      confirmButtonText: "บันทึก",
      cancelButtonText: "ยกเลิก",
      closeOnConfirm: false,
      closeOnCancel: true
    }, function (isConfirm) {
      if (isConfirm) {
        var form_research = $("#form_research");
        $.ajax({
          url: "php/research_process.php",
          type: "post", //กำหนดให้มีรูปแบบเป็น post
          data: form_research.serialize() + "&edit_research=edit_research",
          success: function (data) {
            if (data == "yes") {
              swal({
                title: "บันทึกข้อมูลเรียบร้อย",
                text: "",
                type: "success",
                confirmButtonColor: "#26dad2",
                confirmButtonText: "OK"
              }, function () {
                window.location.replace(treatise_type + ".php");
                //window.location.replace("academic_article_edit.php?res_id=" + treatise_id);
              }); // swal
            } // if data == yes
            else {
              console.log(data);
            }
          } // success data
        }); // ajax
      }// isConfirm
      else {
        return false;
      }
    });
  }); // #edit_res button
});

function delete_research(res_id_1, treatise_type_1) {
  swal({
    title: "คุณต้องการลบข้อมูล?",
    text: "",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#26dad2",
    confirmButtonText: "ตกลง",
    cancelButtonText: "ยกเลิก",
    closeOnConfirm: false,
    closeOnCancel: true
  }, function (isConfirm) {
    if (isConfirm) {
      $.ajax({
        url: "php/research_process.php",
        type: "post", //กำหนดให้มีรูปแบบเป็น Json
        data: {
          delete_research: 'delete_research',
          res_id_2: res_id_1,
          treatise_type_2: treatise_type_1
        },
        success: function (data) {
          if (data == "yes") {
            swal({
              title: "ลบข้อมูลเรียบร้อย",
              text: "",
              type: "success",
              confirmButtonColor: "#26dad2",
              confirmButtonText: "OK"
            }, function () {
              window.location.replace(treatise_type_1 + ".php");
            }); // swal
          } // if data == yes
        } // success data
      }); // ajax
    }// isConfirm
    else {
      return false;
    }
  });
}; // #delete button
