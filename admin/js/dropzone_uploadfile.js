Dropzone.autoDiscover = false;

var myDropzone_pdf = new Dropzone("#drop_pdf", {
    url: "php/upload_file.php",
    maxFiles: "1",
    dictMaxFilesExceeded: "สามารถเลือกได้เพียงไฟล์เดียวเท่านั้น",
    addRemoveLinks: true,    //เพิ่มการลบรูปที่เลือก (ไม่ได้ลบไฟล์)
    autoProcessQueue: false, //ปิด auto upload
    dictDefaultMessage: "เพิ่มไฟล์เอกสาร PDF",   //เปลี่ยนข้อความ
    dictCancelUpload: "<i class='mdi mdi-close-circle'></i>", //เปลี่ยนข้อความเป็น Icon
    dictRemoveFile: "<i class='mdi mdi-close-circle'></i>",  //เปลี่ยนข้อความเป็น Icon
    acceptedFiles: ".pdf",
    dictInvalidFileType: "โปรดเลือกไฟล์เอกสาร PDF เท่านั้น",
    maxFilesize: "10",
    dictFileTooBig: "ไฟล์ใหญ่เกินไป โปรดเลือกไฟล์ขนาดไม่เกิน 10 MB",
    //เปลี่ยนชื่อไฟล์
    renameFile: function (file) {
        let num_ran = Math.floor((Math.random() * 999999999) + 1);   //create random number
        let ext = file.name.split('.').pop();   //get สกุลไฟล์
        let newName = num_ran + '.' + ext;  //create new file with new name
        return newName;
    },
    //เอาชื่อไฟล์ที่ตั้งใหม่ออกมา
    init: function () {
        this.on("addedfile", function (file) {
            file.previewElement.appendChild(
                Dropzone.createElement(`<input type="hidden" name="file" id="file" value="${file.upload.filename}">`)
            )
        }),
            //เมื่อ error จะแจ้งเตือนตามเรื่องต่างๆ
            this.on("error", function (file, message) {
                swal({
                    title: message,
                    text: "",
                    type: "error",
                    confirmButtonColor: "#26dad2",
                    confirmButtonText: "OK"
                  });
                this.removeFile(file);
            });
    },
});

$(function () {

  $('#save_research').click(function () {
    var treatise_type = $('#treatise_type').val();

    var emptyName = false;
    var emptySurname = false;
    var emptyAcademic_rank = false;
    var emptyName_title = false;

    // เช็คค่าว่างของชื่อเจ้าของบทความ
    if ($.each($('.name'), function () {
      if ($(this).val() == '') {
        emptyName = true;
      }
    })
    );

    if ($.each($('.surname'), function () {
      if ($(this).val() == '') {
        emptySurname = true;
      }
    })
    );

    if ($.each($('.academic_rank'), function () {
      if ($(this).val() == '') {
        emptyAcademic_rank = true;
      }
    })
    );

    if ($.each($('.name_title'), function () {
      if ($(this).val() == '') {
        emptyName_title = true;
      }
    })
    );

    if (emptyName) {
      $.each($('.name'), function () {
        if ($(this).val() == '') {
          $(this).focus();
          swal({
            title: "กรุณาใส่ชื่อเจ้าของบทความ",
            text: "",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#26dad2",
            confirmButtonText: 'OK!',
          });
          return false;
        }
      });
    }
    else if (emptySurname) {
      $.each($('.surname'), function () {
        if ($(this).val() == '') {
          $(this).focus();
          swal({
            title: "กรุณาใส่นามสกุลเจ้าของบทความ",
            text: "",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#26dad2",
            confirmButtonText: 'OK!',
          });
          return false;
        }
      });
    }
    else if (emptyAcademic_rank) {
      $.each($('.academic_rank'), function () {
        if ($(this).val() == '') {
          $(this).focus();
          swal({
            title: "กรุณาเลือกตำแหน่งทางวิชาการของเจ้าของบทความ",
            text: "",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#26dad2",
            confirmButtonText: 'OK!',
          });
          return false;
        }
      });
    }
    else if (emptyName_title) {
      $.each($('.name_title'), function () {
        if ($(this).val() == '') {
          $(this).focus();
          swal({
            title: "กรุณาเลือกคำนำหน้าชื่อของเจ้าของบทความ",
            text: "",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#26dad2",
            confirmButtonText: 'OK!',
          });
          return false;
        }
      });
    }
    else if ($('#event_name').val() == '') {
      $('#event_name').focus();
      swal({
        title: "กรุณาใส่ชื่องานที่จัดขึ้น",
        text: "",
        type: "error",
        showCancelButton: false,
        confirmButtonColor: "#26dad2",
        confirmButtonText: 'OK!',
      });
      return false;
    }
    else if ($('#grade').val() == '') {
      $('#grade').focus();
      swal({
        title: "กรุณาเลือกระดับของผลงาน",
        text: "",
        type: "error",
        showCancelButton: false,
        confirmButtonColor: "#26dad2",
        confirmButtonText: 'OK!',
      });
      return false;
    }
    else if ($('#conference_name').val() == '') {
      $('#conference_name').focus();
      swal({
        title: "กรุณาใส่ชื่อเรื่องของบทความที่นำเสนอ",
        text: "",
        type: "error",
        showCancelButton: false,
        confirmButtonColor: "#26dad2",
        confirmButtonText: 'OK!',
      });
      return false;
    }
    else if ($('#showdate').val() == '') {
      $('#showdate').focus();
      swal({
        title: "กรุณาเลือกวันที่นำเสนอ",
        text: "",
        type: "error",
        showCancelButton: false,
        confirmButtonColor: "#26dad2",
        confirmButtonText: 'OK!',
      });
      return false;
    }
    else {
      swal({
        title: "คุณต้องการบันทึกข้อมูล?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#26dad2",
        confirmButtonText: "บันทึก",
        cancelButtonText: "ยกเลิก",
        closeOnConfirm: false,
        closeOnCancel: true
      }, function (isConfirm) {
        if (isConfirm) {
          var form_research = $("#form_research");
          $.ajax({
            url: "php/research_process.php",
            type: "post", //กำหนดให้มีรูปแบบเป็น post
            data: form_research.serialize() + "&add_research=add_research",
            success: function (data) {
              if (data == "yes") {
                myDropzone_pdf.processQueue()
                swal({
                  title: "บันทึกข้อมูลเรียบร้อย",
                  text: "",
                  type: "success",
                  confirmButtonColor: "#26dad2",
                  confirmButtonText: "OK"
                }, function () {
                  window.location.replace(treatise_type + ".php");
                }); // swal
              }
              else {
                console.log(data);
              }
            } // success data
          }); // ajax
        }// isConfirm
        else {
          return false;
        }
      });
    }
  }); // #save_research button

  $('#edit_res').click(function () {
    var treatise_id = $('#treatise_id').val();
    var treatise_type = $('#treatise_type').val();
    swal({
      title: "คุณต้องการบันทึกข้อมูล?",
      text: "",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#26dad2",
      confirmButtonText: "บันทึก",
      cancelButtonText: "ยกเลิก",
      closeOnConfirm: false,
      closeOnCancel: true
    }, function (isConfirm) {
      if (isConfirm) {
        var form_research = $("#form_research");
        $.ajax({
          url: "php/research_process.php",
          type: "post", //กำหนดให้มีรูปแบบเป็น post
          data: form_research.serialize() + "&edit_research=edit_research",
          success: function (data) {
            if (data == "yes") {
              myDropzone_pdf.processQueue()
              swal({
                title: "บันทึกข้อมูลเรียบร้อย",
                text: "",
                type: "success",
                confirmButtonColor: "#26dad2",
                confirmButtonText: "OK"
              }, function () {
                window.location.replace(treatise_type + ".php");
                //window.location.replace("academic_article_edit.php?res_id=" + treatise_id);
              }); // swal
            } // if data == yes
            else {
              console.log(data);
            }
          } // success data
        }); // ajax
      }// isConfirm
      else {
        return false;
      }
    });
  }); // #edit_res button
});

//log response ที่ได้ออกมาดู
// myDropzone_pdf.on("success", function(file){
//     console.log(file);
//     let res = JSON.parse(file.xhr.response)
//     console.log(res);
//     file.previewElement.appendChild(
//         Dropzone.createElement(`<input type="text" name="img_name" id="file_pdf" value="${res.file_name}">`)
//     )
// })

//ปุ่มเอาไว้ test อยู่ที่ test_dropzone.php
// $('#gogo').click(function () {
//     myDropzone_pdf.processQueue()
//     console.log("Gogo Clicked");
// })
