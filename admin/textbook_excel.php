<?php

include("../inc/connect.php");
include('php/checklogin.php');
require '../assets/plugins/phpspreadsheet/vendor/autoload.php';
$db = new database();
$con = $db->connect();
error_reporting(0);

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

$treatise_type = "textbook";
$search_name = $_GET['search_name'];
$search_surname = $_GET['search_surname'];
$start_year = $_GET['start_year'];
$end_year = $_GET['end_year'];

if ($search_name) {
    $sql_textbook = "SELECT author.*,textbook.* ";
    $sql_textbook .= "FROM author,textbook ";
    $sql_textbook .= "WHERE author.treatise_id = textbook.item_id ";
    $sql_textbook .= "and author.name = '$search_name' ";
    $sql_textbook .= "and author.surname = '$search_surname' ";
    $sql_textbook .= "and treatise_type = 'textbook' ";
    if ($start_year) {
        $sql_textbook .= "and year_make >= '$start_year' ";
    } else if ($end_year) {
        $sql_textbook .= "and year_make <= '$end_year' ";
    } else if ($start_year && $end_year) {
        $sql_textbook .= "and year_make >= '$start_year' and year_make <= '$end_year' ";
    }
} else {
    $sql_textbook = "SELECT * FROM textbook ";
    if ($start_year) {
        $sql_textbook .= "WHERE year_make >= '$start_year' ";
    } else if ($end_year) {
        $sql_textbook .= "WHERE year_make <= '$end_year' ";
    } else if ($start_year && $end_year) {
        $sql_textbook .= "WHERE year_make >= '$start_year' and year_make <= '$end_year' ";
    }
}

$sql_textbook .= "ORDER BY textbook_name";

$query_textbook = $con->query($sql_textbook);

$spreadsheet = new Spreadsheet();
$spreadsheet->getDefaultStyle()->getFont()->setName('TH Sarabun New');
$spreadsheet->getDefaultStyle()->getFont()->setSize(18);
$spreadsheet->getDefaultStyle()->getAlignment()->setHorizontal('center');
$spreadsheet->getActiveSheet()->getStyle('A1:G1')->getFont()->setSize(22);
$spreadsheet->getActiveSheet()->getStyle('A1:G1')->getFont()->setBold('Bold');
$spreadsheet->getActiveSheet()->getStyle('G')->getAlignment()->setHorizontal('left');
for ($col = 'A'; $col != 'H'; $col++) {
    $spreadsheet->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
}
$sheet = $spreadsheet->getActiveSheet();
$sheet->setCellValue('A1', 'เรื่องที่');
$sheet->setCellValue('B1', 'ชื่อตำรา');
$sheet->setCellValue('C1', 'จำนวนหน้า');
$sheet->setCellValue('D1', 'ปีที่แต่ง');
$sheet->setCellValue('E1', 'จำนวนบท');
$sheet->setCellValue('F1', 'ISBN');
$sheet->setCellValue('G1', 'รายชื่อผู้แต่ง');

if ($query_textbook->num_rows > 0) {
    $result_row = 1;
    $i = 1;
    while ($result_textbook = $query_textbook->fetch_object()) {

        $item_id = $result_textbook->item_id;
        //fetch author
        $sql_author = "SELECT * FROM author WHERE treatise_type = '$treatise_type' AND treatise_id = '$item_id' ORDER BY author_id";
        $query_author = $con->query($sql_author);
        $num_author = $query_author->num_rows;

        $textbook_name = $result_textbook->textbook_name;
        $year_make = $result_textbook->year_make;
        $page = $result_textbook->page;
        $lesson = $result_textbook->lesson;
        $isbn = $result_textbook->isbn;

        $rowNum = $result_row + 1;

        $sheet->setCellValue('A' . $rowNum, $i);
        $sheet->setCellValue('B' . $rowNum, $textbook_name);
        $sheet->setCellValue('C' . $rowNum, $page);
        $sheet->setCellValue('D' . $rowNum, $year_make);
        $sheet->setCellValue('E' . $rowNum, $lesson);
        $sheet->setCellValue('F' . $rowNum, $isbn);
        $sheet->setCellValue('G' . $rowNum, "จำนวนผู้แต่ง " . $num_author . " คน");

        $ii = 1;
        while ($result_author = $query_author->fetch_object()) {
            $author_name = $result_author->name;
            $author_surname = $result_author->surname;
            $author_name_title = $result_author->name_title;
            $author_academic_rank = $result_author->academic_rank;
            if ($author_name_title == "dr") {
                $name_title_show = "ดร.";
            } else {
                $name_title_show = "";
            }

            if ($author_academic_rank == "pro") {
                $academic_rank_show = "ศ.";
            } else if ($author_academic_rank == "asso") {
                $academic_rank_show = "ร.ศ.";
            } else if ($author_academic_rank == "assis") {
                $academic_rank_show = "ผ.ศ.";
            } else if ($author_academic_rank == "lec") {
                if ($name_title_show == "ดร.") {
                    $academic_rank_show = "อาจารย์ ";
                } else {
                    $academic_rank_show = "อาจารย์ ";
                }
            }
            $author_row = $result_row + 2;
            $sheet->setCellValue('G' . $author_row, "คนที่ " . $ii . " : " . $academic_rank_show . $name_title_show . " " . $author_name . " " . $author_surname);
            $result_row++;
            $ii++;
        }
        $result_row++;
        $i++;
    }
}

$filename = 'ตำรา-' . time() . '.xlsx';
// Redirect output to a client's web browser (Xlsx)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.

$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
