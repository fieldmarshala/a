<?php
include("../inc/connect.php");
include('php/checklogin.php');
$db = new database();
$con = $db->connect();

error_reporting(E_ALL ^ E_NOTICE);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon.png">
    <title><?php include("inc/title.php"); ?></title>
    <!-- Bootstrap Core CSS -->
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- page CSS -->
    <link href="../assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/switchery/dist/switchery.min.css" rel="stylesheet" />
    <!--Sweet alerts CSS & JS-->
    <script src="../assets/plugins/sweetalert/sweetalert.min.js"></script>
    <link href="../assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
    <!-- Custom CSS -->
    <link href="css/report.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="css/colors/blue.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header fix-sidebar card-no-border " data-spy="scroll">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <?php include("inc/topmenu.php") ?>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <?php include("inc/left_sidebar.php"); ?>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <?php $page_title = "ผู้ใช้งาน"; ?>
            <?php $page_detail = "จัดการข้อมูลผู้ใช้งาน" ?>
            <?php $page_title_active = "breadcrumb-item"; ?>
            <?php $page_detail_active = "breadcrumb-item active"; ?>
            <?php include("inc/breadcrumb.php"); ?>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-outline-info">
                            <div class="card-header">
                                <h4 class="m-b-0 text-white"><?= $page_detail; ?></h4>
                            </div>
                            <div class="card-body">
                                <div class="form-body">
                                    <?php
                                    //ดึงชื่อผู้แต่งจาก table author
                                    $sql_get_author = "SELECT DISTINCT author.name, author.surname FROM author ORDER BY 'name'";
                                    $query_get_author = $con->query($sql_get_author);
                                    $showName = $_GET['search_name'];
                                    $Getsearch_name = explode(" ", $showName);
                                    $search_name = $Getsearch_name[0];
                                    $search_surname = $Getsearch_name[1];
                                    $start_year = $_GET['start_year'];
                                    $end_year = $_GET['end_year'];
                                    $today = DateTimeNew(date("Y/m/d"));
                                    if (($start_year != '') && ($end_year != '')) {
                                        if ($start_year > $end_year) {
                                            echo "<script language = javascript>
            swal({
                title: 'โปรดกรอกปีให้ถูกต้อง',
                text: '`เริ่มปีที่` ไม่สามารถมากกว่า `ถึงปีที่` ได้',
                type: 'error',
                confirmButtonColor: 'lightgreen',
                confirmButtonText: 'OK'
              }, function () {
                window.location.replace('index.php');
              });
        </script>";
                                        }
                                    }
                                    ?>
                                    <form method="get">
                                        <div class="row p-t-10">
                                            <div class="form-group col-md-2">
                                                <label for="">เลือกชื่อผู้แต่งที่ต้องการ</label>
                                                <select class="form-control select2 " name="search_name" id="search_name">
                                                    <option value="" <?php echo "selected"; ?>>โปรดเลือก ชื่อ - นามสกุล</option>
                                                    <?php while ($result_get_author = $query_get_author->fetch_object()) :; ?>
                                                        <option value="<?php echo "$result_get_author->name $result_get_author->surname"; ?>" <?php if ($showName == $result_get_author->name . " " . $result_get_author->surname) {
                                                                                                                                                    echo "selected";
                                                                                                                                                } ?>><?php echo "$result_get_author->name $result_get_author->surname"; ?></option>
                                                    <?php endwhile; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row p-t-10">
                                            <div class="col-md-1">
                                                <div id="form_start_year" class="form-group">
                                                    <label id="label_start_year" class="control-label">เริ่มปีที่</label>
                                                    <input type="text" id="start_year" name="start_year" data-date-language="th-th" class="form-control datepicker" value="<?php if ($_GET['start_year']) echo $_GET['start_year']; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <div id="form_end_year" class="form-group">
                                                    <label id="label_end_year" class="control-label">ถึงปีที่</label>
                                                    <input type="text" id="end_year" name="end_year" data-date-language="th-th" class="form-control datepicker" value="<?php echo $_GET['end_year']; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        $research_status = $_GET['research_status'];
                                        if ($research_status == "complete") {
                                            $research_status_show = "เสร็จสิ้น";
                                        } else if ($research_status == "processing") {
                                            $research_status_show = "กำลังดำเนินการ";
                                        } else if ($research_status == "start") {
                                            $research_status_show = "เริ่มโครงร่างวิจัย";
                                        } else if ($research_status == "overdue") {
                                            $research_status_show = "เกินกำหนดระยะเวลา";
                                        }
                                        ?>
                                        <div class="row p-t-10">
                                            <div class="col-md-3">
                                                <label id="label_research_status" class="control-label">เลือกสถานะงานวิจัย *เฉพาะงานวิจัย*</label>
                                                <select id="research_status" name="research_status" class="form-control select2 col-md-8">
                                                    <?php
                                                    if ($research_status == '') { ?>
                                                        <option value="" selected>--------- เลือก ---------</option>
                                                    <?php } else { ?>
                                                        <option value="<?= $_GET['research_status']; ?>" selected><?= $research_status_show; ?></option>
                                                    <?php } ?>
                                                    <option value="start">เริ่มโครงร่างวิจัย</option>
                                                    <option value="processing">กำลังดำเนินการ</option>
                                                    <option value="complete">เสร็จสิ้น</option>
                                                    <option value="overdue">เกินกำหนดระยะเวลา</option>
                                                </select>
                                            </div>
                                        </div><br>
                                        <div class="row p-t-10">
                                            <div class="col-md-1">
                                                <div id="btn_submit" class="form-group">
                                                    <button type="submit" class="btn btn-success col-md-12">ค้นหา</button>
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <div id="btn_clear" class="form-group">
                                                    <a href="index.php"><button type="button" class="btn btn-danger col-md-12">Reset</button></a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- <hr> -->
                                    <!--/row-->

                                    <!-- Scroll 
                                    <div class="row p-t-10">
                                        <div class="col-md-1">
                                            <div id="form_start_year" class="form-group">
                                                <a href="#div_research"><button type="button" class="btn btn-submit col-md-12">Research</button></a>
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div id="form_end_year" class="form-group">
                                                <a href="#div_creative"><button type="button" class="btn btn-warning col-md-12">Creative</button></a>
                                            </div>
                                        </div>
                                    </div>
                                    -->

                                    <!-- show research -->
                                    <div class="row p-t-10" id="div_research">
                                        <?php
                                        if ($search_name) {
                                            $sql_research = "SELECT author.*,research.* ";
                                            $sql_research .= "FROM author,research ";
                                            $sql_research .= "WHERE author.treatise_id = research.item_id ";
                                            $sql_research .= "and author.name = '$search_name' ";
                                            $sql_research .= "and author.surname = '$search_surname' ";
                                            $sql_research .= "and treatise_type = 'research' ";
                                            if ($research_status) {
                                                if ($research_status == "overdue") {
                                                    $sql_research .= "and (research_status = 'start' or research_status = 'processing') ";
                                                    $sql_research .= "and research_processdate < '$today' ";
                                                } else {
                                                    $sql_research .= "and research_status = '$research_status' ";
                                                }
                                            }
                                        } else {
                                            $sql_research = "SELECT * FROM research ";
                                            if ($research_status) {
                                                if ($research_status == "overdue") {
                                                    $sql_research .= "WHERE (research_status = 'start' or research_status = 'processing') ";
                                                    $sql_research .= "and research_processdate < '$today' ";
                                                } else {
                                                    $sql_research .= "WHERE research_status = '$research_status' ";
                                                }
                                            }
                                        }

                                        $sql_research .= "ORDER BY research_name";

                                        $query_research = $con->query($sql_research);
                                        $i = 1;
                                        //ถ้ามีผลลัพธ์ ถึงจะโชว์ table
                                        $num_research = $query_research->num_rows;

                                        ?>
                                        <div class="col-md-12 divTable">
                                            <table id="research" class="table table-bordered table-striped DataTableTH">
                                                <h3><b><u>งานวิจัย</u></b></h3>
                                                <h3 class="card-title"><a href="research_add.php" class="btn waves-effect waves-light btn-primary">เพิ่มข้อมูลงานวิจัย <i class="fa fa-lg fa-plus"></i></a></h3>
                                                <div class="row p-t-10">
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <h4><label for="" style="color:red">*Export ข้อมูลตามผลลัพธ์ในตารางด้านล่าง*</label></h4>
                                                            <a href="research_excel.php?search_name=<?= $search_name; ?>&search_surname=<?= $search_surname; ?>&research_status=<?= $research_status; ?>"><button type="button" class="btn btn-warning col-md-6" id="export_research">Export (.xls)</button></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <thead class="thh">
                                                    <tr>
                                                        <th width="1%">#</th>
                                                        <th width="10%">ชื่อผลงาน</th>
                                                        <th width="3%">สถานะงานวิจัย</th>
                                                        <th width="2%">วันที่เริ่มโครงร่างวิจัย</th>
                                                        <th width="2%">ระยะเวลาดำเนินการวิจัย</th>
                                                        <th width="1%">แก้ไข</th>
                                                        <th width="1%">ลบ</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    while ($result_research = $query_research->fetch_object()) {
                                                        $r_status = $result_research->research_status;
                                                        if ($r_status == "complete") {
                                                            $r_status_show = "เสร็จสิ้น";
                                                        } else if ($r_status == "processing") {
                                                            $r_status_show = "กำลังดำเนินการ";
                                                        } else if ($r_status == "start") {
                                                            $r_status_show = "เริ่มโครงร่างวิจัย";
                                                        }
                                                        ?>
                                                        <tr>
                                                            <td><?= $i; ?></td>
                                                            <td><?= $result_research->research_name; ?></td>
                                                            <td><?= $r_status_show; ?></td>
                                                            <td><?= DateConvertBase($result_research->research_startdate); ?></td>
                                                            <td><?= DateConvertBase($result_research->research_processdate); ?></td>
                                                            <td align=center><a href="research_edit.php?research_id=<?= $result_research->item_id; ?>" class="btn waves-effect waves-light btn-warning"><i class="icon-note fa-lg"></i></a></td>
                                                            <td align=center><button id="delete" type="button" class="btn waves-effect waves-light btn-danger" onclick="return delete_research('<?php echo $result_research->item_id; ?>','research')"><i class="fa fa-trash-o fa-lg"></i></button></td>
                                                        </tr>
                                                        <?php $i++;
                                                    } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <!-- show creative -->
                                    <div class="row p-t-10" id="div_creative">
                                        <?php

                                        if ($search_name) {
                                            $sql_creative = "SELECT author.*,creative.* ";
                                            $sql_creative .= "FROM author,creative ";
                                            $sql_creative .= "WHERE author.treatise_id = creative.item_id ";
                                            $sql_creative .= "and author.name = '$search_name' ";
                                            $sql_creative .= "and author.surname = '$search_surname' ";
                                            $sql_creative .= "and treatise_type = 'creative' ";
                                            if ($start_year) {
                                                $sql_creative .= "and year_make >= '$start_year' ";
                                            } else if ($end_year) {
                                                $sql_creative .= "and year_make <= '$end_year' ";
                                            } else if ($start_year && $end_year) {
                                                $sql_creative .= "and year_make >= '$start_year' and year_make <= '$end_year' ";
                                            }
                                        } else {
                                            $sql_creative = "SELECT * FROM creative ";
                                            if ($start_year) {
                                                $sql_creative .= "WHERE year_make >= '$start_year' ";
                                            } else if ($end_year) {
                                                $sql_creative .= "WHERE year_make <= '$end_year' ";
                                            } else if ($start_year && $end_year) {
                                                $sql_creative .= "WHERE year_make >= '$start_year' and year_make <= '$end_year' ";
                                            }
                                        }

                                        $sql_creative .= "ORDER BY creative_name";

                                        $query_creative = $con->query($sql_creative);
                                        $i = 1;
                                        //ถ้ามีผลลัพธ์ ถึงจะโชว์ table
                                        $num_creative = $query_creative->num_rows;

                                        ?>
                                        <div class="col-md-12 divTable">
                                            <table id="creative" class="table table-bordered table-striped DataTableTH">
                                                <h3><b><u>ผลงานสร้างสรรค์</u></b></h3>
                                                <h3 class="card-title"><a href="creative_add.php" class="btn waves-effect waves-light btn-primary">เพิ่มข้อมูลผลงานสร้างสรรค์ <i class="fa fa-lg fa-plus"></i></a></h3>
                                                <div class="row p-t-10">
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <h4><label for="" style="color:red">*Export ข้อมูลตามผลลัพธ์ในตารางด้านล่าง*</label></h4>
                                                            <a href="creative_excel.php?search_name=<?= $search_name; ?>&search_surname=<?= $search_surname; ?>&start_year=<?= $start_year; ?>&end_year=<?= $end_year; ?>"><button type="button" class="btn btn-warning col-md-6" id="export_creative">Export (.xls)</button></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <thead class="thh">
                                                    <tr>
                                                        <th width="1%">#</th>
                                                        <th width="10%">ชื่อผลงาน</th>
                                                        <th width="3%">ปีที่ผลิต</th>
                                                        <th width="1%">แก้ไข</th>
                                                        <th width="1%">ลบ</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    while ($result_creative = $query_creative->fetch_object()) {
                                                        ?>
                                                        <tr>
                                                            <td><?= $i; ?></td>
                                                            <td><?= $result_creative->creative_name; ?></td>
                                                            <td><?= $result_creative->year_make; ?></td>
                                                            <td align=center><a href="creative_edit.php?research_id=<?= $result_creative->item_id; ?>" class="btn waves-effect waves-light btn-warning"><i class="icon-note fa-lg"></i></a></td>
                                                            <td align=center><button id="delete" type="button" class="btn waves-effect waves-light btn-danger" onclick="return delete_research('<?php echo $result_creative->item_id; ?>','creative')"><i class="fa fa-trash-o fa-lg"></i></button></td>
                                                        </tr>
                                                        <?php $i++;
                                                    } ?>
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>

                                    <!-- show book -->
                                    <div class="row p-t-10" id="div_book">
                                        <?php

                                        if ($search_name) {
                                            $sql_book = "SELECT author.*,book.* ";
                                            $sql_book .= "FROM author,book ";
                                            $sql_book .= "WHERE author.treatise_id = book.item_id ";
                                            $sql_book .= "and author.name = '$search_name' ";
                                            $sql_book .= "and author.surname = '$search_surname' ";
                                            $sql_book .= "and treatise_type = 'book' ";
                                            if ($start_year) {
                                                $sql_book .= "and year_make >= '$start_year' ";
                                            } else if ($end_year) {
                                                $sql_book .= "and year_make <= '$end_year' ";
                                            } else if ($start_year && $end_year) {
                                                $sql_book .= "and year_make >= '$start_year' and year_make <= '$end_year' ";
                                            }
                                        } else {
                                            $sql_book = "SELECT * FROM book ";
                                            if ($start_year) {
                                                $sql_book .= "WHERE year_make >= '$start_year' ";
                                            } else if ($end_year) {
                                                $sql_book .= "WHERE year_make <= '$end_year' ";
                                            } else if ($start_year && $end_year) {
                                                $sql_book .= "WHERE year_make >= '$start_year' and year_make <= '$end_year' ";
                                            }
                                        }

                                        $sql_book .= "ORDER BY book_name";

                                        $query_book = $con->query($sql_book);
                                        $i = 1;
                                        //ถ้ามีผลลัพธ์ ถึงจะโชว์ table
                                        $num_book = $query_book->num_rows;

                                        ?>
                                        <div class="col-md-12 divTable">
                                            <table id="book" class="table table-bordered table-striped DataTableTH">
                                                <h3><b><u>หนังสือ</u></b></h3>
                                                <h3 class="card-title"><a href="book_add.php" class="btn waves-effect waves-light btn-primary">เพิ่มข้อมูลหนังสือ <i class="fa fa-lg fa-plus"></i></a></h3>
                                                <div class="row p-t-10">
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <h4><label for="" style="color:red">*Export ข้อมูลตามผลลัพธ์ในตารางด้านล่าง*</label></h4>
                                                            <a href="book_excel.php?search_name=<?= $search_name; ?>&search_surname=<?= $search_surname; ?>&start_year=<?= $start_year; ?>&end_year=<?= $end_year; ?>"><button type="button" class="btn btn-warning col-md-6" id="export_book">Export (.xls)</button></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <thead class="thh">
                                                    <tr>
                                                        <th width="1%">#</th>
                                                        <th width="10%">ชื่อผลงาน</th>
                                                        <th width="3%">ปีที่แต่ง</th>
                                                        <th width="1%">แก้ไข</th>
                                                        <th width="1%">ลบ</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    while ($result_book = $query_book->fetch_object()) {
                                                        ?>
                                                        <tr>
                                                            <td><?= $i; ?></td>
                                                            <td><?= $result_book->book_name; ?></td>
                                                            <td><?= $result_book->year_make; ?></td>
                                                            <td align=center><a href="book_edit.php?research_id=<?= $result_book->item_id; ?>" class="btn waves-effect waves-light btn-warning"><i class="icon-note fa-lg"></i></a></td>
                                                            <td align=center><button id="delete" type="button" class="btn waves-effect waves-light btn-danger" onclick="return delete_research('<?php echo $result_book->item_id; ?>','book')"><i class="fa fa-trash-o fa-lg"></i></button></td>
                                                        </tr>
                                                        <?php $i++;
                                                    } ?>
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>

                                    <!-- show textbook -->
                                    <div class="row p-t-10" id="div_textbook">
                                        <?php

                                        if ($search_name) {
                                            $sql_textbook = "SELECT author.*,textbook.* ";
                                            $sql_textbook .= "FROM author,textbook ";
                                            $sql_textbook .= "WHERE author.treatise_id = textbook.item_id ";
                                            $sql_textbook .= "and author.name = '$search_name' ";
                                            $sql_textbook .= "and author.surname = '$search_surname' ";
                                            $sql_textbook .= "and treatise_type = 'textbook' ";
                                            if ($start_year) {
                                                $sql_textbook .= "and year_make >= '$start_year' ";
                                            } else if ($end_year) {
                                                $sql_textbook .= "and year_make <= '$end_year' ";
                                            } else if ($start_year && $end_year) {
                                                $sql_textbook .= "and year_make >= '$start_year' and year_make <= '$end_year' ";
                                            }
                                        } else {
                                            $sql_textbook = "SELECT * FROM textbook ";
                                            if ($start_year) {
                                                $sql_textbook .= "WHERE year_make >= '$start_year' ";
                                            } else if ($end_year) {
                                                $sql_textbook .= "WHERE year_make <= '$end_year' ";
                                            } else if ($start_year && $end_year) {
                                                $sql_textbook .= "WHERE year_make >= '$start_year' and year_make <= '$end_year' ";
                                            }
                                        }

                                        $sql_textbook .= "ORDER BY textbook_name";

                                        $query_textbook = $con->query($sql_textbook);
                                        $i = 1;
                                        //ถ้ามีผลลัพธ์ ถึงจะโชว์ table
                                        $num_textbook = $query_textbook->num_rows;

                                        ?>
                                        <div class="col-md-12 divTable">
                                            <table id="textbook" class="table table-bordered table-striped DataTableTH">
                                                <h3><b><u>ตำรา</u></b></h3>
                                                <h3 class="card-title"><a href="textbook_add.php" class="btn waves-effect waves-light btn-primary">เพิ่มข้อมูลตำรา <i class="fa fa-lg fa-plus"></i></a></h3>
                                                <div class="row p-t-10">
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <h4><label for="" style="color:red">*Export ข้อมูลตามผลลัพธ์ในตารางด้านล่าง*</label></h4>
                                                            <a href="textbook_excel.php?search_name=<?= $search_name; ?>&search_surname=<?= $search_surname; ?>&start_year=<?= $start_year; ?>&end_year=<?= $end_year; ?>"><button type="button" class="btn btn-warning col-md-6" id="export_textbook">Export (.xls)</button></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <thead class="thh">
                                                    <tr>
                                                        <th width="1%">#</th>
                                                        <th width="10%">ชื่อผลงาน</th>
                                                        <th width="3%">ปีที่แต่ง</th>
                                                        <th width="1%">แก้ไข</th>
                                                        <th width="1%">ลบ</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    while ($result_textbook = $query_textbook->fetch_object()) {
                                                        ?>
                                                        <tr>
                                                            <td><?= $i; ?></td>
                                                            <td><?= $result_textbook->textbook_name; ?></td>
                                                            <td><?= $result_textbook->year_make; ?></td>
                                                            <td align=center><a href="textbook_edit.php?research_id=<?= $result_textbook->item_id; ?>" class="btn waves-effect waves-light btn-warning"><i class="icon-note fa-lg"></i></a></td>
                                                            <td align=center><button id="delete" type="button" class="btn waves-effect waves-light btn-danger" onclick="return delete_research('<?php echo $result_textbook->item_id; ?>','textbook')"><i class="fa fa-trash-o fa-lg"></i></button></td>
                                                        </tr>
                                                        <?php $i++;
                                                    } ?>
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>

                                    <!-- show teaching -->
                                    <div class="row p-t-10" id="div_teaching">
                                        <?php
                                        if ($search_name) {
                                            $sql_teaching = "SELECT author.*,teaching.* ";
                                            $sql_teaching .= "FROM author,teaching ";
                                            $sql_teaching .= "WHERE author.treatise_id = teaching.item_id ";
                                            $sql_teaching .= "and author.name = '$search_name' ";
                                            $sql_teaching .= "and author.surname = '$search_surname' ";
                                            $sql_teaching .= "and treatise_type = 'teaching' ";
                                            if ($start_year) {
                                                $sql_teaching .= "and year_make >= '$start_year' ";
                                            } else if ($end_year) {
                                                $sql_teaching .= "and year_make <= '$end_year' ";
                                            } else if ($start_year && $end_year) {
                                                $sql_teaching .= "and year_make >= '$start_year' and year_make <= '$end_year' ";
                                            }
                                        } else {
                                            $sql_teaching = "SELECT * FROM teaching ";
                                            if ($start_year) {
                                                $sql_teaching .= "WHERE year_make >= '$start_year' ";
                                            } else if ($end_year) {
                                                $sql_teaching .= "WHERE year_make <= '$end_year' ";
                                            } else if ($start_year && $end_year) {
                                                $sql_teaching .= "WHERE year_make >= '$start_year' and year_make <= '$end_year' ";
                                            }
                                        }

                                        $sql_teaching .= "ORDER BY teaching_name";

                                        $query_teaching = $con->query($sql_teaching);
                                        $i = 1;
                                        //ถ้ามีผลลัพธ์ ถึงจะโชว์ table
                                        $num_teaching = $query_teaching->num_rows;

                                        ?>
                                        <div class="col-md-12 divTable">
                                            <table id="teaching" class="table table-bordered table-striped DataTableTH">
                                                <h3><b><u>เอกสารประกอบการสอน</u></b></h3>
                                                <h3 class="card-title"><a href="teaching_add.php" class="btn waves-effect waves-light btn-primary">เพิ่มข้อมูลผลเอกสารประกอบการสอน <i class="fa fa-lg fa-plus"></i></a></h3>
                                                <div class="row p-t-10">
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <h4><label for="" style="color:red">*Export ข้อมูลตามผลลัพธ์ในตารางด้านล่าง*</label></h4>
                                                            <a href="teaching_excel.php?search_name=<?= $search_name; ?>&search_surname=<?= $search_surname; ?>&start_year=<?= $start_year; ?>&end_year=<?= $end_year; ?>"><button type="button" class="btn btn-warning col-md-6" id="export_teaching">Export (.xls)</button></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <thead class="thh">
                                                    <tr>
                                                        <th width="1%">#</th>
                                                        <th width="10%">ชื่อผลงาน</th>
                                                        <th width="3%">ปีที่แต่ง</th>
                                                        <th width="1%">แก้ไข</th>
                                                        <th width="1%">ลบ</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    while ($result_teaching = $query_teaching->fetch_object()) {
                                                        ?>
                                                        <tr>
                                                            <td><?= $i; ?></td>
                                                            <td><?= $result_teaching->teaching_name; ?></td>
                                                            <td><?= $result_teaching->year_make; ?></td>
                                                            <td align=center><a href="teaching_edit.php?research_id=<?= $result_teaching->item_id; ?>" class="btn waves-effect waves-light btn-warning"><i class="icon-note fa-lg"></i></a></td>
                                                            <td align=center><button id="delete" type="button" class="btn waves-effect waves-light btn-danger" onclick="return delete_research('<?php echo $result_teaching->item_id; ?>','teaching')"><i class="fa fa-trash-o fa-lg"></i></button></td>
                                                        </tr>
                                                        <?php $i++;
                                                    } ?>
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>

                                    <!-- show academic_article -->
                                    <div class="row p-t-10" id="div_academic_article">
                                        <?php

                                        if ($search_name) {
                                            $sql_academic_article = "SELECT author.*,academic_article.* ";
                                            $sql_academic_article .= "FROM author,academic_article ";
                                            $sql_academic_article .= "WHERE author.treatise_id = academic_article.item_id ";
                                            $sql_academic_article .= "and author.name = '$search_name' ";
                                            $sql_academic_article .= "and author.surname = '$search_surname' ";
                                            $sql_academic_article .= "and treatise_type = 'academic_article' ";
                                            if ($start_year) {
                                                $sql_academic_article .= "and year_make >= '$start_year' ";
                                            } else if ($end_year) {
                                                $sql_academic_article .= "and year_make <= '$end_year' ";
                                            } else if ($start_year && $end_year) {
                                                $sql_academic_article .= "and year_make >= '$start_year' and year_make <= '$end_year' ";
                                            }
                                        } else {
                                            $sql_academic_article = "SELECT * FROM academic_article ";
                                            if ($start_year) {
                                                $sql_academic_article .= "WHERE year_make >= '$start_year' ";
                                            } else if ($end_year) {
                                                $sql_academic_article .= "WHERE year_make <= '$end_year' ";
                                            } else if ($start_year && $end_year) {
                                                $sql_academic_article .= "WHERE year_make >= '$start_year' and year_make <= '$end_year' ";
                                            }
                                        }

                                        $sql_academic_article .= "ORDER BY academic_article_name";

                                        $query_academic_article = $con->query($sql_academic_article);
                                        $i = 1;
                                        //ถ้ามีผลลัพธ์ ถึงจะโชว์ table
                                        $num_academic_article = $query_academic_article->num_rows;

                                        ?>
                                        <div class="col-md-12 divTable">
                                            <table id="academic_article" class="table table-bordered table-striped DataTableTH">
                                                <h3><b><u>บทความวิชาการ</u></b></h3>
                                                <h3 class="card-title"><a href="academic_article_add.php" class="btn waves-effect waves-light btn-primary">เพิ่มข้อมูลบทความวิชาการ <i class="fa fa-lg fa-plus"></i></a></h3>
                                                <div class="row p-t-10">
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <h4><label for="" style="color:red">*Export ข้อมูลตามผลลัพธ์ในตารางด้านล่าง*</label></h4>
                                                            <a href="academic_article_excel.php?search_name=<?= $search_name; ?>&search_surname=<?= $search_surname; ?>&start_year=<?= $start_year; ?>&end_year=<?= $end_year; ?>"><button type="button" class="btn btn-warning col-md-6" id="export_academic_article">Export (.xls)</button></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <thead class="thh">
                                                    <tr>
                                                        <th width="1%">#</th>
                                                        <th width="10%">ชื่อผลงาน</th>
                                                        <th width="3%">ปีที่แต่ง</th>
                                                        <th width="1%">แก้ไข</th>
                                                        <th width="1%">ลบ</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    while ($result_academic_article = $query_academic_article->fetch_object()) {
                                                        ?>
                                                        <tr>
                                                            <td><?= $i; ?></td>
                                                            <td><?= $result_academic_article->academic_article_name; ?></td>
                                                            <td><?= $result_academic_article->year_make; ?></td>
                                                            <td align=center><a href="academic_article_edit.php?research_id=<?= $result_academic_article->item_id; ?>" class="btn waves-effect waves-light btn-warning"><i class="icon-note fa-lg"></i></a></td>
                                                            <td align=center><button id="delete" type="button" class="btn waves-effect waves-light btn-danger" onclick="return delete_research('<?php echo $result_academic_article->item_id; ?>','academic_article')"><i class="fa fa-trash-o fa-lg"></i></button></td>
                                                        </tr>
                                                        <?php $i++;
                                                    } ?>
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>

                                    <!-- show research_article -->
                                    <div class="row p-t-10" id="div_research_article">
                                        <?php

                                        if ($search_name) {
                                            $sql_research_article = "SELECT author.*,research_article.* ";
                                            $sql_research_article .= "FROM author,research_article ";
                                            $sql_research_article .= "WHERE author.treatise_id = research_article.item_id ";
                                            $sql_research_article .= "and author.name = '$search_name' ";
                                            $sql_research_article .= "and author.surname = '$search_surname' ";
                                            $sql_research_article .= "and treatise_type = 'research_article' ";
                                            if ($start_year) {
                                                $sql_research_article .= "and year_make >= '$start_year' ";
                                            } else if ($end_year) {
                                                $sql_research_article .= "and year_make <= '$end_year' ";
                                            } else if ($start_year && $end_year) {
                                                $sql_research_article .= "and year_make >= '$start_year' and year_make <= '$end_year' ";
                                            }
                                        } else {
                                            $sql_research_article = "SELECT * FROM research_article ";
                                            if ($start_year) {
                                                $sql_research_article .= "WHERE year_make >= '$start_year' ";
                                            } else if ($end_year) {
                                                $sql_research_article .= "WHERE year_make <= '$end_year' ";
                                            } else if ($start_year && $end_year) {
                                                $sql_research_article .= "WHERE year_make >= '$start_year' and year_make <= '$end_year' ";
                                            }
                                        }

                                        $sql_research_article .= "ORDER BY research_article_name";

                                        $query_research_article = $con->query($sql_research_article);
                                        $i = 1;
                                        //ถ้ามีผลลัพธ์ ถึงจะโชว์ table
                                        $num_research_article = $query_research_article->num_rows;

                                        ?>
                                        <div class="col-md-12 divTable">
                                            <table id="research_article" class="table table-bordered table-striped DataTableTH">
                                                <h3><b><u>บทความวิจัย</u></b></h3>
                                                <h3 class="card-title"><a href="research_article_add.php" class="btn waves-effect waves-light btn-primary">เพิ่มข้อมูลบทความวิจัย <i class="fa fa-lg fa-plus"></i></a></h3>
                                                <div class="row p-t-10">
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <h4><label for="" style="color:red">*Export ข้อมูลตามผลลัพธ์ในตารางด้านล่าง*</label></h4>
                                                            <a href="research_article_excel.php?search_name=<?= $search_name; ?>&search_surname=<?= $search_surname; ?>&start_year=<?= $start_year; ?>&end_year=<?= $end_year; ?>"><button type="button" class="btn btn-warning col-md-6" id="export_research_article">Export (.xls)</button></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <thead class="thh">
                                                    <tr>
                                                        <th width="1%">#</th>
                                                        <th width="10%">ชื่อผลงาน</th>
                                                        <th width="3%">ปีที่แต่ง</th>
                                                        <th width="1%">แก้ไข</th>
                                                        <th width="1%">ลบ</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    while ($result_research_article = $query_research_article->fetch_object()) {
                                                        ?>
                                                        <tr>
                                                            <td><?= $i; ?></td>
                                                            <td><?= $result_research_article->research_article_name; ?></td>
                                                            <td><?= $result_research_article->year_make; ?></td>
                                                            <td align=center><a href="research_article_edit.php?research_id=<?= $result_research_article->item_id; ?>" class="btn waves-effect waves-light btn-warning"><i class="icon-note fa-lg"></i></a></td>
                                                            <td align=center><button id="delete" type="button" class="btn waves-effect waves-light btn-danger" onclick="return delete_research('<?php echo $result_research_article->item_id; ?>','research_article')"><i class="fa fa-trash-o fa-lg"></i></button></td>
                                                        </tr>
                                                        <?php $i++;
                                                    } ?>
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>

                                    <!-- show conference -->
                                    <div class="row p-t-10" id="div_conference">
                                        <?php
                                        if ($search_name) {
                                            $sql_conference = "SELECT author.*,conference.* ";
                                            $sql_conference .= "FROM author,conference ";
                                            $sql_conference .= "WHERE author.treatise_id = conference.item_id ";
                                            $sql_conference .= "and author.name = '$search_name' ";
                                            $sql_conference .= "and author.surname = '$search_surname' ";
                                            $sql_conference .= "and treatise_type = 'conference' ";
                                            if ($start_year) {
                                                $sql_conference .= "and year_make >= '$start_year' ";
                                            } else if ($end_year) {
                                                $sql_conference .= "and year_make <= '$end_year' ";
                                            } else if ($start_year && $end_year) {
                                                $sql_conference .= "and year_make >= '$start_year' and year_make <= '$end_year' ";
                                            }
                                        } else {
                                            $sql_conference = "SELECT * FROM conference ";
                                            if ($start_year) {
                                                $sql_conference .= "WHERE year_make >= '$start_year' ";
                                            } else if ($end_year) {
                                                $sql_conference .= "WHERE year_make <= '$end_year' ";
                                            } else if ($start_year && $end_year) {
                                                $sql_conference .= "WHERE year_make >= '$start_year' and year_make <= '$end_year' ";
                                            }
                                        }

                                        $sql_conference .= "ORDER BY conference_name";

                                        $query_conference = $con->query($sql_conference);
                                        $i = 1;
                                        //ถ้ามีผลลัพธ์ ถึงจะโชว์ table
                                        $num_conference = $query_conference->num_rows;

                                        ?>
                                        <div class="col-md-12 divTable" id="tta">
                                            <!-- show conference -->
                                            <table id="conference" class="table table-bordered table-striped DataTableTH">
                                                <h3><b><u>การนำเสนอในที่ประชุม</u></b></h3>
                                                <h3 class="card-title"><a href="conference_add.php" class="btn waves-effect waves-light btn-primary">เพิ่มข้อมูลการนำเสนอในที่ประชุม <i class="fa fa-lg fa-plus"></i></a></h3>
                                                <div class="row p-t-10">
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <h4><label for="" style="color:red">*Export ข้อมูลตามผลลัพธ์ในตารางด้านล่าง*</label></h4>
                                                            <a href="conference_excel.php?search_name=<?= $search_name; ?>&search_surname=<?= $search_surname; ?>&start_year=<?= $start_year; ?>&end_year=<?= $end_year; ?>"><button type="button" class="btn btn-warning col-md-6" id="export_conference">Export (.xls)</button></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <thead class="thh">
                                                    <tr>
                                                        <th width="1%">#</th>
                                                        <th width="10%">ชื่อผลงาน</th>
                                                        <th width="3%">ปีที่แต่ง</th>
                                                        <th width="1%">แก้ไข</th>
                                                        <th width="1%">ลบ</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    while ($result_conference = $query_conference->fetch_object()) {
                                                        ?>
                                                        <tr>
                                                            <td><?= $i; ?></td>
                                                            <td><?= $result_conference->conference_name; ?></td>
                                                            <td>ปีที่แต่ง</td>
                                                            <td align=center><a href="conference_edit.php?conference_id=<?= $result_conference->item_id; ?>" class="btn waves-effect waves-light btn-warning"><i class="icon-note fa-lg"></i></a></td>
                                                            <td align=center><button id="delete" type="button" class="btn waves-effect waves-light btn-danger" onclick="return delete_research('<?php echo $result_conference->item_id; ?>','conference')"><i class="fa fa-trash-o fa-lg"></i></button></td>
                                                        </tr>
                                                        <?php $i++;
                                                    } ?>
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>

                                    <!--/row-->
                                </div> <!-- form body -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <?php include("inc/right_sidebar.php"); ?>
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"><?php include("inc/footer.php"); ?></footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="../assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../assets/plugins/bootstrap/js/popper.min.js"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="../assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <script src="../assets/plugins/switchery/dist/switchery.min.js"></script>
    <script src="../assets/plugins/select2/dist/js/select2.full.min.js" type="text/javascript"></script>
    <!-- This is data table -->
    <script src="../assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="../assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="../assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- JS Datepicker TH -->
    <script src="js/bootstrap-datepicker/js/bootstrap-datepicker-thai.js"></script>
    <script src="js/bootstrap-datepicker/locales/bootstrap-datepicker.th.js"></script>
    <!-- user js -->
    <script src="js/user.js"></script>
    <script src="js/login.js"></script>
    <script src="js/report.js"></script>
    <script src="js/research.js"></script>
    <script src="js/creative.js"></script>
    <script src="js/research_article.js"></script>
    <script src="js/academic_article.js"></script>
    <script src="js/book.js"></script>
    <script src="js/textbook.js"></script>
    <script src="js/teaching.js"></script>
    <script src="js/conference.js"></script>
    <button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>
    <script>
        //input form
        var search_name = '<?php echo $search_name; ?>';
        var start_year = '<?php echo $start_year; ?>';
        var end_year = '<?php echo $end_year; ?>';
        var research_status = '<?php echo $research_status; ?>';

        //check num of result
        var num_research = '<?php echo $num_research; ?>';
        var num_creative = '<?php echo $num_creative; ?>';
        var num_academic_article = '<?php echo $num_academic_article; ?>';
        var num_research_article = '<?php echo $num_research_article; ?>';
        var num_conference = '<?php echo $num_conference; ?>';
        var num_book = '<?php echo $num_book; ?>';
        var num_textbook = '<?php echo $num_textbook; ?>';
        var num_teaching = '<?php echo $num_teaching; ?>';

        if (num_research >= 1) {
            $("#export_research").attr('disabled', false);
        } else {
            $("#export_research").attr('disabled', true);
        }

        if (num_creative >= 1) {
            $("#export_creative").attr('disabled', false);
        } else {
            $("#export_creative").attr('disabled', true);
        }

        if (num_academic_article >= 1) {
            $("#export_academic_article").attr('disabled', false);
        } else {
            $("#export_academic_article").attr('disabled', true);
        }

        if (num_research_article >= 1) {
            $("#export_research_article").attr('disabled', false);
        } else {
            $("#export_research_article").attr('disabled', true);
        }

        if (num_conference >= 1) {
            $("#export_conference").attr('disabled', false);
        } else {
            $("#export_conference").attr('disabled', true);
        }

        if (num_book >= 1) {
            $("#export_book").attr('disabled', false);
        } else {
            $("#export_book").attr('disabled', true);
        }

        if (num_textbook >= 1) {
            $("#export_textbook").attr('disabled', false);
        } else {
            $("#export_textbook").attr('disabled', true);
        }

        if (num_teaching >= 1) {
            $("#export_teaching").attr('disabled', false);
        } else {
            $("#export_teaching").attr('disabled', true);
        }

        /*if (((search_name !== '') || (research_status !== '')) && (num_research >= 1)) {
            $("#export_research").attr('disabled', false);
        } else {
            $("#export_research").attr('disabled', true);
        }*/
    </script>
</body>

</html>