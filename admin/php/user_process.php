<?php
error_reporting(E_ALL ^ E_NOTICE);
include("../../inc/connect.php");
$db = new database();
$con = $db->connect();

if (isset($_POST['add_user'])) {
	$user_name_title = $_POST['user_name_title'];
	$user_academic_rank = $_POST['user_academic_rank'];
	$user_name = $_POST['user_name'];
	$user_surname = $_POST['user_surname'];
	$user_birthday = DateConvert($_POST['user_birthday']);
	$user_education = $_POST['user_education'];
	$user_tel = $_POST['user_tel'];
	$user_email = $_POST['user_email'];
	$user_local_address = $_POST['user_local_address'];
	$user_local_province = $_POST['user_local_province'];
	$user_address = $_POST['user_address'];
	$user_province = $_POST['user_province'];
	$user_id_card = $_POST['user_id_card'];
	$user_username = $_POST['user_username'];
	$user_password = $_POST['user_password'];
	$user_status = $_POST['user_status'];

	$user_start_working = DateConvert($_POST['user_start_working']);
	$user_work_type = $_POST['user_work_type'];
	$user_budget_type = $_POST['user_budget_type'];
	$user_type = $_POST['user_type'];

	$img_name = $_POST['img_name'];	// ชื่อรูปโปรไฟล์

	$sql_user = "SELECT * FROM user WHERE user_username = '$user_username'";
	$query_user = $con->query($sql_user);
	$num_user = $query_user->num_rows;	//เช็คว่ามี username ซ้ำหรือไม่

	if ($num_user == 0) {
		$sql = "INSERT INTO user";
		$sql .= "(name_title, academic_rank, name, surname, education, ";
		$sql .= "phone, email, local_address, local_province, address, ";
		$sql .= "province, id_card, user_username, user_password, user_status, birthday) ";
		$sql .= " VALUES ";
		$sql .= "('$user_name_title', '$user_academic_rank', '$user_name', '$user_surname', '$user_education', ";
		$sql .= " '$user_tel', '$user_email', '$user_local_address', '$user_local_province', '$user_address', ";
		$sql .= " '$user_province', '$user_id_card', '$user_username', '$user_password', '$user_status', '$user_birthday')";
		$con->query($sql);

		$sql_get_user_id = "SELECT * FROM user WHERE user_username = '$user_username'";
		$query_get_user_id = $con->query($sql_get_user_id);
		$num_get_user_id = $query_get_user_id->fetch_object();	//get user_id ของ user ที่เพิ่มเข้าไปล่าสุด

		$sql_work = "INSERT INTO work_detail";
		$sql_work .= "(work_type, user_type, budget_type, user_id, start_working)";
		$sql_work .= "VALUES ";
		$sql_work .= "('$user_work_type', '$user_type', '$user_budget_type','$num_get_user_id->user_id', '$user_start_working')";
		$con->query($sql_work);

		$sql_img = "INSERT INTO profile_img";
		$sql_img .= "(user_id, image_name)";
		$sql_img .= "VALUES ";
		$sql_img .=  "('$num_get_user_id->user_id', '$img_name')";
		$con->query($sql_img);

		echo "yes";
	} else {
		echo "no";
	}
}

if (isset($_POST['edit_user'])) {

	$user_id = $_POST['user_id'];
	$user_academic_rank = $_POST['user_academic_rank'];
	$user_name_title = $_POST['user_name_title'];
	$user_name = $_POST['user_name'];
	$user_surname = $_POST['user_surname'];
	$user_birthday = DateConvert($_POST['user_birthday']);
	$user_education = $_POST['user_education'];
	$user_tel = $_POST['user_tel'];
	$user_email = $_POST['user_email'];
	$user_local_address = $_POST['user_local_address'];
	$user_local_province = $_POST['user_local_province'];
	$user_address = $_POST['user_address'];
	$user_province = $_POST['user_province'];
	$user_id_card = $_POST['user_id_card'];
	$user_username = $_POST['user_username'];
	$user_password = $_POST['user_password'];
	$user_status = $_POST['user_status'];

	$user_start_working = DateConvert($_POST['user_start_working']);
	$user_work_type = $_POST['user_work_type'];
	$user_budget_type = $_POST['user_budget_type'];
	$user_type = $_POST['user_type'];

	$old_pic = $_POST['old_pic'];
	$new_pic = $_POST['img_name'];

	$sql = "UPDATE user SET ";
	$sql .= "academic_rank = '$user_academic_rank', ";
	$sql .= "name_title = '$user_name_title', ";
	$sql .= "name = '$user_name', ";
	$sql .= "surname = '$user_surname', ";
	$sql .= "birthday = '$user_birthday', ";
	$sql .= "education = '$user_education', ";
	$sql .= "phone = '$user_tel', ";
	$sql .= "email = '$user_email', ";
	$sql .= "local_address = '$user_local_address', ";
	$sql .= "local_province = '$user_local_province', ";
	$sql .= "address = '$user_address', ";
	$sql .= "province = '$user_province', ";
	$sql .= "id_card = '$user_id_card', ";
	$sql .= "user_username = '$user_username', ";
	$sql .= "user_password = '$user_password', ";
	$sql .= "user_status = '$user_status' ";
	$sql .= "WHERE user_id = '$user_id' ";
	$con->query($sql);

	$sql_work = "UPDATE work_detail SET ";
	$sql_work .= "start_working = '$user_start_working', ";
	$sql_work .= "work_type = '$user_work_type', ";
	$sql_work .= "budget_type = '$user_budget_type', ";
	$sql_work .= "user_type = '$user_type' ";
	$sql_work .= "WHERE user_id = '$user_id' ";
	$con->query($sql_work);

	//เช็คว่ามีการอัพรูปมาใหม่หรือไม่
	if ((isset($new_pic)) && ($new_pic !== '')) {
		$sql_img = "UPDATE profile_img SET image_name = '$new_pic' WHERE user_id = '$user_id'";
		$con->query($sql_img);

		if ((isset($old_pic)) && ($old_pic !== '')) {
			$files = '../../images/profiles/' . $old_pic;

			if (file_exists($files))	// image_exists คือฟังก์ชัน เช็คว่าไฟล์หรือ directory มีอยู่หรือไม่
			{
				unlink($files);
			}
		}
	}
	echo "yes";
}

if (isset($_POST['delete_user'])) {
	$user_id = $_POST['user_id_2'];

	$sql = "DELETE FROM user WHERE user_id = '$user_id' ";
	$con->query($sql);

	$sql_work = "DELETE FROM work_detail WHERE user_id = '$user_id' ";
	$con->query($sql_work);

	$sql_get_image = "SELECT * FROM profile_img WHERE user_id = '$user_id'";
	$query_image = $con->query($sql_get_image);
	$result_image = $query_image->fetch_object();
	$get_img_name = $result_image->image_name;

	$sql_img = "DELETE FROM profile_img WHERE user_id = '$user_id' ";
	$con->query($sql_img);

	$files = '../../images/profiles/' . $get_img_name;

	if (file_exists($files))	// image_exists คือฟังก์ชัน เช็คว่าไฟล์หรือ directory มีอยู่หรือไม่
	{
		unlink($files);
	}

	echo "yes";
}

if (isset($_POST['delete_author'])) {
	$author_id = $_POST['author_id_2'];

	$sql = "DELETE FROM author WHERE author_id = '$author_id' ";
	$con->query($sql);

	echo "yes";
}

if (isset($_POST['add_author_from_edit'])) {

	$treatise_type = $_POST['treatise_type'];
	$treatise_id = $_POST['treatise_id'];
	$name_add = $_POST['add_name'];
	if ((isset($name_add)) && ($name_add != '')) {
		$academic_rank_add = $_POST['add_academic_rank'];
		$name_title_add = $_POST['add_name_title'];
		$surname_add = $_POST['add_surname'];

		$sql_author_add = "INSERT INTO author";
		$sql_author_add .= "(name_title, academic_rank,";
		$sql_author_add .= "name, surname, treatise_type,";
		$sql_author_add .= "treatise_id)";
		$sql_author_add .= "VALUES ";
		$sql_author_add .= "('$name_title_add', '$academic_rank_add',";
		$sql_author_add .= "'$name_add', '$surname_add', '$treatise_type',";
		$sql_author_add .= "'$treatise_id')";
		$con->query($sql_author_add);
	} else {
		echo "no";
	}
	echo "yes";
}
