<?php
require "../../assets/plugins/SimpleImage/src/claviska/SimpleImage.php";

if ($_FILES['file']['tmp_name']) {
  /*$ext = explode(".", $_FILES['file']['name']);  //ตัดเชื่อไฟล์ออกเอาแต่สกุลไฟล์
  $exten = end($ext); //$ext มีข้อมูลเป็น array ใช้ end เพื่อเอา array ช่องสุดท้าย จะได้สกุลไฟล์
  $filename = uniqid() . "." . $exten; */ //เปลี่ยนชื่อไฟล์ใหม่โดยการใช้การสุ่มตัวเลข + สกุลไฟล์
  $tmp_name = $_FILES['file']['tmp_name'];
  $name = $_FILES['file']['name'];
  $img_path = "../../images/profiles/".$name; //ที่เก็บไฟล์
  try {
    // Create a new SimpleImage object
    $image = new \claviska\SimpleImage();

    // Magic! ✨
    $image
    //->fromFile("../images/user.jpg")
      ->fromFile($_FILES['file']['tmp_name'])                     // load image.jpg
      ->autoOrient()                              // adjust orientation based on exif data
      ->resize(100, 0)                          // resize to 320x200 pixels
      ->toFile($img_path);     // convert to PNG and save a copy to new-image.png

    //ส่ง response ไป file.xhr.response
    echo json_encode([
      "success" => true,
      "tmp_name" => "$tmp_name",  //ส่ง img path ไปดู ถ้าอัพโหลดสำเร็จ
      "img_path" => "$img_path",  //path
      "img_name" => "$name",  //ส่ง img_name ไปใช้ ถ้าอัพโหลดสำเร็จ
      "msg" => "OK Uploaded"
    ]);
    // And much more! 💪
  } catch (Exception $err) {
    // Handle errors
    echo json_encode([
      "success" => false,
      "msg" => $err->getMessage()
    ]);
  }
}
