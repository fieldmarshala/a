<?php
error_reporting(E_ALL ^ E_NOTICE);
include("../../inc/connect.php");
include('../php/checklogin.php');
$db = new database();
$con = $db->connect();

if (isset($_POST['add_research'])) {

	//ชนิดของผลงาน งานวิจัย, หนังสือ, เอกสารประกอบการสอน ฯลฯ
	$treatise_type = $_POST['treatise_type'];

	//เพิ่มบทความวิชาการ
	if ($treatise_type == "academic_article") {

		$academic_article_name = $_POST['academic_article_name'];
		$page = $_POST['page'];
		$year_make = $_POST['year_make'];
		$journal_name = $_POST['journal_name'];
		$year = $_POST['year'];
		$copy_no = $_POST['copy_no'];
		$start_month = $_POST['start_month'];
		$end_month = $_POST['end_month'];
		$issn = $_POST['issn'];

		$sql = "INSERT INTO academic_article";
		$sql .= "(academic_article_name, page, year_make, journal_name,";
		$sql .= "year, copy_no, start_month, end_month,";
		$sql .= "issn, user_id)";
		$sql .= "VALUES ";
		$sql .= "('$academic_article_name', '$page', '$year_make', '$journal_name',";
		$sql .= "'$year', '$copy_no', '$start_month', '$end_month',";
		$sql .= "'$issn','$a_id')";	//ถ้าเป็น admin id ที่กำลังล็อกอินอยู่ = a_id --> php/checklogin.php
		$con->query($sql);

		//ดึง research_id / treatise_id ของผลงานล่าสุดเพื่อไปอ้างอิงถึงความเป็นเจ้าของของผลงานที่ table author
		$sql_get_treatise_id = "SELECT * FROM academic_article WHERE academic_article_name = '$academic_article_name'";
	}

	//เพิ่มบทความวิจัย
	if ($treatise_type == "research_article") {

		$research_article_name = $_POST['research_article_name'];
		$page = $_POST['page'];
		$year_make = $_POST['year_make'];
		$journal_name = $_POST['journal_name'];
		$year = $_POST['year'];
		$copy_no = $_POST['copy_no'];
		$start_month = $_POST['start_month'];
		$end_month = $_POST['end_month'];
		$issn = $_POST['issn'];

		$sql = "INSERT INTO research_article";
		$sql .= "(research_article_name, page, year_make, journal_name,";
		$sql .= "year, copy_no, start_month, end_month,";
		$sql .= "issn, user_id)";
		$sql .= "VALUES ";
		$sql .= "('$research_article_name', '$page', '$year_make', '$journal_name',";
		$sql .= "'$year', '$copy_no', '$start_month', '$end_month',";
		$sql .= "'$issn','$a_id')";	//ถ้าเป็น admin id ที่กำลังล็อกอินอยู่ = a_id --> php/checklogin.php
		$con->query($sql);

		//ดึง research_id / treatise_id ของผลงานล่าสุดเพื่อไปอ้างอิงถึงความเป็นเจ้าของของผลงานที่ table author
		$sql_get_treatise_id = "SELECT * FROM research_article WHERE research_article_name = '$research_article_name'";
	}

	//เพิ่มเอกสารประกอบการสอน
	if ($treatise_type == "teaching") {

		$teaching_name = $_POST['teaching_name'];
		$page = $_POST['page'];
		$year_make = $_POST['year_make'];
		$faculty = $_POST['faculty'];

		$sql = "INSERT INTO teaching";
		$sql .= "(teaching_name, page, year_make, faculty, user_id)";
		$sql .= "VALUES ";
		$sql .= "('$teaching_name', '$page', '$year_make', '$faculty', '$a_id')";
		$con->query($sql);

		//ดึง research_id / treatise_id ของผลงานล่าสุดเพื่อไปอ้างอิงถึงความเป็นเจ้าของของผลงานที่ table author
		$sql_get_treatise_id = "SELECT * FROM teaching WHERE teaching_name = '$teaching_name'";
	}

	//เพิ่มตำรา
	if ($treatise_type == "textbook") {

		$textbook_name = $_POST['textbook_name'];	//ชื่อตำรา
		$page = $_POST['page'];	//จำนวนหน้า
		$year_make = $_POST['year_make'];	//ปีที่แต่ง
		$lesson = $_POST['lesson'];	//จำนวนบท
		$isbn = $_POST['isbn'];	//ISBN

		$sql = "INSERT INTO textbook";
		$sql .= "(textbook_name, page, year_make, lesson, isbn, user_id)";
		$sql .= "VALUES ";
		$sql .= "('$textbook_name', '$page', '$year_make', '$lesson', '$isbn', '$a_id')";
		$con->query($sql);

		//ดึง research_id / treatise_id ของผลงานล่าสุดเพื่อไปอ้างอิงถึงความเป็นเจ้าของของผลงานที่ table author
		$sql_get_treatise_id = "SELECT * FROM textbook WHERE textbook_name = '$textbook_name'";
	}

	//เพิ่มหนังสือ
	if ($treatise_type == "book") {

		$book_name = $_POST['book_name'];	//ชื่อหนังสือ
		$page = $_POST['page'];	//จำนวนหน้า
		$year_make = $_POST['year_make'];	//ปีที่พิมพ์
		$printery = $_POST['printery'];	//สำนักพิมพ์
		$lesson = $_POST['lesson'];	//จำนวนบท
		$copy_no = $_POST['copy_no'];	//พิมพ์ครั้งที่
		$isbn = $_POST['isbn'];

		$sql = "INSERT INTO book";
		$sql .= "(book_name, page, year_make, printery,";
		$sql .= "lesson, copy_no,";
		$sql .= "isbn, user_id)";
		$sql .= "VALUES ";
		$sql .= "('$book_name', '$page', '$year_make', '$printery',";
		$sql .= "'$lesson', '$copy_no',";
		$sql .= "'$isbn','$a_id')";	//ถ้าเป็น admin id ที่กำลังล็อกอินอยู่ = a_id --> php/checklogin.php
		$con->query($sql);

		//ดึง research_id / treatise_id ของผลงานล่าสุดเพื่อไปอ้างอิงถึงความเป็นเจ้าของของผลงานที่ table author
		$sql_get_treatise_id = "SELECT * FROM book WHERE book_name = '$book_name'";
	}

	//เพิ่มงานวิจัย
	if ($treatise_type == "research") {

		$research_name = $_POST['research_name'];
		$research_status = $_POST['research_status'];
		$research_budget_from = $_POST['research_budget_from'];
		$research_budget_year = $_POST['research_budget_year'];
		$research_budget = $_POST['research_budget'];
		$research_startdate = DateConvert($_POST['research_startdate']);
		$research_enddate = DateConvert($_POST['research_enddate']);
		$research_processdate = DateConvert($_POST['research_processdate']);

		$sql_work = "INSERT INTO research";
		$sql_work .= "(research_name, research_processdate,";
		$sql_work .= "research_status, research_budget_from, research_budget_year,";
		$sql_work .= "research_budget, research_startdate, research_enddate, user_id)";
		$sql_work .= "VALUES ";
		$sql_work .= "('$research_name', '$research_processdate',";
		$sql_work .= "'$research_status', '$research_budget_from', '$research_budget_year',";
		$sql_work .= "'$research_budget', '$research_startdate', '$research_enddate', '$a_id')";	//ถ้าเป็น admin id ที่กำลังล็อกอินอยู่ = a_id --> php/checklogin.php
		$con->query($sql_work);

		//ดึง research_id / treatise_id ของผลงานล่าสุดเพื่อไปอ้างอิงถึงความเป็นเจ้าของของผลงานที่ table author
		$sql_get_treatise_id = "SELECT * FROM research WHERE research_name = '$research_name'";
	}

	//เพิ่มการนำเสนอในที่ประชุม
	if ($treatise_type == "conference") {

		$conference_name = $_POST['conference_name'];
		$event_name = $_POST['event_name'];
		$grade = $_POST['grade'];
		$showdate = DateConvert($_POST['showdate']);
		$year_make = GetYear($_POST['showdate']);
		$file = $_POST['file'];

		$sql = "INSERT INTO conference";
		$sql .= "(conference_name, grade, event_name, showdate, file_pdf, year_make, user_id)";
		$sql .= "VALUES ";
		$sql .= "('$conference_name', '$grade', '$event_name', '$showdate', '$file', '$year_make', '$a_id')";
		$con->query($sql);

		//ดึง research_id / treatise_id ของผลงานล่าสุดเพื่อไปอ้างอิงถึงความเป็นเจ้าของของผลงานที่ table author
		$sql_get_treatise_id = "SELECT * FROM conference WHERE conference_name = '$conference_name' AND event_name = '$event_name'";
	}

	//เพิ่มเอกสารประกอบการสอน
	if ($treatise_type == "creative") {

		$creative_name = $_POST['creative_name'];
		$reward = $_POST['reward'];
		$year_make = $_POST['year_make'];

		$sql = "INSERT INTO creative";
		$sql .= "(creative_name, reward, year_make, user_id)";
		$sql .= "VALUES ";
		$sql .= "('$creative_name', '$reward', '$year_make', '$a_id')";
		$con->query($sql);

		//ดึง research_id / treatise_id ของผลงานล่าสุดเพื่อไปอ้างอิงถึงความเป็นเจ้าของของผลงานที่ table author
		$sql_get_treatise_id = "SELECT * FROM creative WHERE creative_name = '$creative_name'";
	}

	$query_get_treatise_id = $con->query($sql_get_treatise_id);
	$result_get_treatise_id = $query_get_treatise_id->fetch_object();
	$treatise_id = $result_get_treatise_id->item_id;

	//loop for เพิ่มผู้แต่ง
	for ($i = 0; $i <= 7; $i++) {
		$name_title = $_POST['name_title'];
		if (isset($name_title[$i])) {
			$academic_rank = $_POST['academic_rank'];
			$name = $_POST['name'];
			$surname = $_POST['surname'];

			$sql_author = "INSERT INTO author";
			$sql_author .= "(name_title, academic_rank,";
			$sql_author .= "name, surname, treatise_type,";
			$sql_author .= "treatise_id)";
			$sql_author .= "VALUES ";
			$sql_author .= "('$name_title[$i]', '$academic_rank[$i]',";
			$sql_author .= "'$name[$i]', '$surname[$i]', '$treatise_type',";
			$sql_author .= "'$treatise_id')";
			$con->query($sql_author);
		}
	}

	echo "yes";
}

if (isset($_POST['edit_research'])) {

	//ชนิดของผลงาน งานวิจัย, หนังสือ, เอกสารประกอบการสอน ฯลฯ
	$treatise_type = $_POST['treatise_type'];
	$treatise_id = $_POST['treatise_id'];

	//แก้ไขบทความวิชาการ
	if ($treatise_type == "academic_article") {

		$academic_article_name = $_POST['academic_article_name'];
		$page = $_POST['page'];
		$year_make = $_POST['year_make'];
		$journal_name = $_POST['journal_name'];
		$year = $_POST['year'];
		$copy_no = $_POST['copy_no'];
		$start_month = $_POST['start_month'];
		$end_month = $_POST['end_month'];
		$issn = $_POST['issn'];

		$sql = "UPDATE academic_article SET ";

		$sql .= "academic_article_name = '$academic_article_name', ";
		$sql .= "page = '$page', ";
		$sql .= "year_make = '$year_make', ";
		$sql .= "journal_name = '$journal_name', ";
		$sql .= "year = '$year', ";
		$sql .= "copy_no = '$copy_no', ";
		$sql .= "start_month = '$start_month', ";
		$sql .= "end_month = '$end_month', ";
		$sql .= "issn = '$issn' ";

		$sql .= "WHERE item_id = '$treatise_id' ";
		$con->query($sql);
	}

	//แก้ไขบทความวิจัย
	if ($treatise_type == "research_article") {

		$research_article_name = $_POST['research_article_name'];
		$page = $_POST['page'];
		$year_make = $_POST['year_make'];
		$journal_name = $_POST['journal_name'];
		$year = $_POST['year'];
		$copy_no = $_POST['copy_no'];
		$start_month = $_POST['start_month'];
		$end_month = $_POST['end_month'];
		$issn = $_POST['issn'];

		$sql = "UPDATE research_article SET ";

		$sql .= "research_article_name = '$research_article_name', ";
		$sql .= "page = '$page', ";
		$sql .= "year_make = '$year_make', ";
		$sql .= "journal_name = '$journal_name', ";
		$sql .= "year = '$year', ";
		$sql .= "copy_no = '$copy_no', ";
		$sql .= "start_month = '$start_month', ";
		$sql .= "end_month = '$end_month', ";
		$sql .= "issn = '$issn' ";

		$sql .= "WHERE item_id = '$treatise_id' ";
		$con->query($sql);
	}

	//แก้ไขเอกสารประกอบการสอน
	if ($treatise_type == "teaching") {

		$teaching_name = $_POST['teaching_name'];
		$page = $_POST['page'];
		$year_make = $_POST['year_make'];
		$faculty = $_POST['faculty'];

		$sql = "UPDATE teaching SET ";

		$sql .= "teaching_name = '$teaching_name', ";
		$sql .= "page = '$page', ";
		$sql .= "year_make = '$year_make', ";
		$sql .= "faculty = '$faculty' ";

		$sql .= "WHERE item_id = '$treatise_id' ";
		$con->query($sql);
	}

	//แก้ไขตำรา
	if ($treatise_type == "textbook") {

		$textbook_name = $_POST['textbook_name'];	//ชื่อตำรา
		$page = $_POST['page'];	//จำนวนหน้า
		$year_make = $_POST['year_make'];	//ปีที่แต่ง
		$lesson = $_POST['lesson'];	//จำนวนบท
		$isbn = $_POST['isbn'];	//ISBN

		$sql = "UPDATE textbook SET ";

		$sql .= "textbook_name = '$textbook_name', ";
		$sql .= "page = '$page', ";
		$sql .= "year_make = '$year_make', ";
		$sql .= "lesson = '$lesson', ";
		$sql .= "isbn = '$isbn' ";

		$sql .= "WHERE item_id = '$treatise_id' ";
		$con->query($sql);
	}

	//แก้ไขหนังสือ
	if ($treatise_type == "book") {

		$book_name = $_POST['book_name'];	//ชื่อหนังสือ
		$page = $_POST['page'];	//จำนวนหน้า
		$year_make = $_POST['year_make'];	//ปีที่พิมพ์
		$printery = $_POST['printery'];	//สำนักพิมพ์
		$lesson = $_POST['lesson'];	//จำนวนบท
		$copy_no = $_POST['copy_no'];	//พิมพ์ครั้งที่
		$isbn = $_POST['isbn'];

		$sql = "UPDATE book SET ";

		$sql .= "book_name = '$book_name', ";
		$sql .= "page = '$page', ";
		$sql .= "year_make = '$year_make', ";
		$sql .= "printery = '$printery', ";
		$sql .= "lesson = '$lesson', ";
		$sql .= "copy_no = '$copy_no', ";
		$sql .= "isbn = '$isbn' ";

		$sql .= "WHERE item_id = '$treatise_id' ";
		$con->query($sql);
	}

	//แก้ไขงานวิจัย
	if ($treatise_type == "research") {

		$research_name = $_POST['research_name'];
		$research_status = $_POST['research_status'];
		$research_budget_from = $_POST['research_budget_from'];
		$research_budget_year = $_POST['research_budget_year'];
		$research_budget = $_POST['research_budget'];
		$research_startdate = DateConvert($_POST['research_startdate']);
		$research_enddate = DateConvert($_POST['research_enddate']);
		$research_processdate = DateConvert($_POST['research_processdate']);

		$sql = "UPDATE research SET ";

		$sql .= "research_name = '$research_name', ";
		$sql .= "research_processdate = '$research_processdate', ";
		$sql .= "research_status = '$research_status', ";
		$sql .= "research_budget_from = '$research_budget_from', ";
		$sql .= "research_budget_year = '$research_budget_year', ";
		$sql .= "research_budget = '$research_budget', ";
		$sql .= "research_startdate = '$research_startdate', ";
		$sql .= "research_enddate = '$research_enddate' ";

		$sql .= "WHERE item_id = '$treatise_id' ";
		$con->query($sql);
	}

	//แก้ไขการนำเสนอในที่ประชุม
	if ($treatise_type == "conference") {

		$conference_name = $_POST['conference_name'];
		$event_name = $_POST['event_name'];
		$grade = $_POST['grade'];
		$showdate = DateConvert($_POST['showdate']);
		$year_make = GetYear($_POST['showdate']);

		$old_file = $_POST['old_file'];
		$new_file = $_POST['file'];

		$sql = "UPDATE conference SET ";

		$sql .= "conference_name = '$conference_name', ";
		$sql .= "event_name = '$event_name', ";
		$sql .= "grade = '$grade', ";
		$sql .= "year_make = '$year_make', ";
		$sql .= "showdate = '$showdate' ";

		$sql .= "WHERE item_id = '$treatise_id' ";
		$con->query($sql);

		//เช็คว่ามีการอัพรูปมาใหม่หรือไม่
		if ((isset($new_file)) && ($new_file !== '')) {
			$sql_file = "UPDATE conference SET file_pdf = '$new_file' WHERE item_id = '$treatise_id'";
			$con->query($sql_file);

			if ((isset($old_file)) && ($old_file !== '')) {
				$files = '../../files/' . $old_file;

				if (file_exists($files))	// image_exists คือฟังก์ชัน เช็คว่าไฟล์หรือ directory มีอยู่หรือไม่
				{
					unlink($files);
				}
			}
		}
	}

	//แก้ไขผลงานสร้างสรรค์
	if ($treatise_type == "creative") {

		$creative_name = $_POST['creative_name'];
		$reward = $_POST['reward'];
		$year_make = $_POST['year_make'];

		$sql = "UPDATE creative SET ";

		$sql .= "creative_name = '$creative_name', ";
		$sql .= "reward = '$reward', ";
		$sql .= "year_make = '$year_make' ";

		$sql .= "WHERE item_id = '$treatise_id' ";
		$con->query($sql);
	}

	// loop for แก้ไขผู้แต่ง
	for ($i = 1; $i <= 10; $i++) {
		$name = $_POST[('name' . $i)];
		if (isset($name)) {
			$academic_rank = $_POST[('academic_rank' . $i)];
			$name_title = $_POST[('name_title' . $i)];
			$name = $_POST[('name' . $i)];
			$surname = $_POST[('surname' . $i)];
			$author_id = $_POST[('author_id' . $i)];

			$sql_author = "UPDATE author SET ";
			$sql_author .= "name_title = '$name_title', ";
			$sql_author .= "academic_rank = '$academic_rank', ";
			$sql_author .= "name = '$name', ";
			$sql_author .= "surname = '$surname' ";
			$sql_author .= "WHERE treatise_type = '$treatise_type' ";
			$sql_author .= "AND treatise_id = '$treatise_id' ";
			$sql_author .= "AND author_id = '$author_id' ";
			$con->query($sql_author);
		}
	}

	echo "yes";
}

if (isset($_POST['delete_research'])) {

	$treatise_type = $_POST['treatise_type_2'];
	$res_id = $_POST['res_id_2'];

	//ลบ pdf ของ conference
	if ($treatise_type == "conference") {

		$sql_get_file = "SELECT * FROM conference WHERE item_id = '$res_id'";
		$query_file = $con->query($sql_get_file);
		$result_file = $query_file->fetch_object();
		$get_file_name = $result_file->file_pdf;

		$files = '../../files/' . $get_file_name;

		if (file_exists($files))	// file_exists คือฟังก์ชัน เช็คว่าไฟล์หรือ directory มีอยู่หรือไม่
		{
			unlink($files);
		}
	}

	$sql_delete_research = "DELETE FROM $treatise_type WHERE item_id = '$res_id' ";
	$con->query($sql_delete_research);

	$sql_delete_author = "DELETE FROM author WHERE treatise_id = '$res_id' AND treatise_type = '$treatise_type'";
	$con->query($sql_delete_author);

	echo "yes";
}
