<?php
include('../inc/connect.php');
include('php/checklogin.php');

//บอกว่านี่คือบทความชนิดไหน
$treatise_type = "teaching";

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon.png">
    <title><?php include("inc/title.php"); ?></title>
    <!-- Bootstrap Core CSS -->
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- page CSS -->
    <link href="../assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/switchery/dist/switchery.min.css" rel="stylesheet" />
    <!--Sweet alert CSS -->
    <link href="../assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="css/colors/blue.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <?php include("inc/topmenu.php") ?>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <?php include("inc/left_sidebar.php"); ?>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <?php $page_title = "เอกสารประกอบการสอน"; ?>
            <?php $page_detail = "เพิ่มข้อมูลเอกสารประกอบการสอน" ?>
            <?php $page_title_active = "breadcrumb-item"; ?>
            <?php $page_detail_active = "breadcrumb-item active"; ?>
            <?php include("inc/breadcrumb.php"); ?>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-outline-info">
                            <div class="card-header">
                                <h4 class="m-b-0 text-white"><?= $page_detail; ?></h4>
                            </div>
                            <div class="card-body">
                                <div id="moreField" class="form-body">
                                    <h3 class="card-title"><a href="<?= $treatise_type; ?>.php" class="btn waves-effect waves-light btn-secondary"><i class="fa fa-lg mdi mdi-chevron-left"></i>กลับหน้าเอกสารประกอบการสอน</a></h3>
                                    <form id="form_research" name="form_research" method="post" action="">
                                        <!--row-->
                                        <div class="row p-t-20">
                                            <div class="col-md-12">
                                                <div id="form_teaching_name" class="form-group">
                                                    <label id="label_teaching_name" class="control-label">ชื่อเอกสารประกอบการสอน</label>
                                                    <!-- ส่ง treatise_type -->
                                                    <input type="hidden" id="treatise_type" name="treatise_type" value="<?= $treatise_type; ?>">
                                                    <input type="text" id="teaching_name" name="teaching_name" class="form-control">
                                                </div>
                                                <div id="repeater">
                                                    <div class="row">
                                                        <div class="col-md-10">
                                                        <label id="label_author_all" class="control-label">ชื่อผู้แต่ง</label>
                                                        </div>
                                                    </div>
                                                    <!-- สร้างตั้งแต่ตรงนี้ -->
                                                    <!-- var items = repeater.find(".items"); -->
                                                    <div class="items">
                                                        <div class="item-content">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-md-2">
                                                                        <select data-skip-name="true" data-name="academic_rank[]" class="form-control academic_rank">
                                                                            <option value="" selected>--- ตำแหน่งทางวิชาการ ---</option>
                                                                            <option value="pro">ศ.</option>
                                                                            <option value="asso">ร.ศ.</option>
                                                                            <option value="assis">ผ.ศ.</option>
                                                                            <option value="lec">อาจารย์</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <select data-skip-name="true" data-name="name_title[]" class="form-control name_title">
                                                                            <option value="" selected>------ คำนำหน้าชื่อ ------</option>
                                                                            <option value="mr">นาย</option>
                                                                            <option value="ms">นางสาว</option>
                                                                            <option value="dr">ดร.</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <input type="text" data-skip-name="true" data-name="name[]" class="form-control name" placeholder="ชื่อ" />
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <input type="text" data-skip-name="true" data-name="surname[]" class="form-control surname" placeholder="นามสกุล" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- จนถึงตรงนี้ -->
                                                </div>
                                                <div id="form_faculty" class="form-group">
                                                    <label id="label_faculty" class="control-label">คณะ</label>
                                                    <select id="faculty" name="faculty" class="form-control select2">
                                                        <option value="" selected>------------ เลือก ------------</option>
                                                        <option value="วิทยาศาสตร์และเทคโนโลยี">วิทยาศาสตร์และเทคโนโลยี</option>
                                                        <option value="วิทยาการจัดการ">วิทยาการจัดการ</option>
                                                        <option value="ครุศาสตร์">ครุศาสตร์</option>
                                                        <option value="มนุษยศาสตร์และสังคมศาสตร์">มนุษยศาสตร์และสังคมศาสตร์</option>
                                                        <option value="วิทยาลัยการดนตรี">วิทยาลัยการดนตรี</option>
                                                        <option value="บัณฑิตวิทยาลัย">บัณฑิตวิทยาลัย</option>
                                                    </select>
                                                </div>
                                                <div id="form_year_make" class="form-group">
                                                    <label id="label_year_make" class="control-label">ปีที่แต่ง (พ.ศ.)</label>
                                                    <input type="text" id="year_make" name="year_make" data-date-language="th-th" class="form-control">
                                                </div>
                                                <div id="form_page" class="form-group">
                                                    <label id="label_page" class="control-label">จำนวนหน้า</label>
                                                    <input type="number" id="page" name="page" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!--/row-->

                                <div class="form-actions">
                                    <button type="button" id="save_research" class="btn btn-success col-md-1">บันทึก</button>
                                    <button onclick="history.go(-1);" class="btn btn-inverse col-md-1">ยกเลิก</button>
                                </div>
                            </div> <!-- form body -->
                        </div> <!-- card body -->
                    </div>
                </div>
            </div>
            <!-- Row -->
            <!-- ============================================================== -->
            <!-- End PAge Content -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Right sidebar -->
            <!-- ============================================================== -->
            <!-- .right-sidebar -->
            <?php include("inc/right_sidebar.php"); ?>
            <!-- ============================================================== -->
            <!-- End Right sidebar -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer"><?php include("inc/footer.php"); ?></footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="../assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../assets/plugins/bootstrap/js/popper.min.js"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="../assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <script src="../assets/plugins/switchery/dist/switchery.min.js"></script>
    <!-- select2 -->
    <script src="../assets/plugins/select2/dist/js/select2.full.min.js" type="text/javascript"></script>
    <!-- Sweet-Alert  -->
    <script src="../assets/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="../assets/plugins/sweetalert/jquery.sweet-alert.custom.js"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <script src="../assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
    <!-- ============================================================== -->
    <!-- Date Picker Plugin JavaScript -->
    <script src="../assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- JS Datepicker TH -->
    <script src="js/bootstrap-datepicker/js/bootstrap-datepicker-thai.js"></script>
    <script src="js/bootstrap-datepicker/locales/bootstrap-datepicker.th.js"></script>
    <!-- Custom js -->
    <script src="js/user.js"></script>
    <script src="js/login.js"></script>
    <script src="js/teaching.js"></script>
    <script src="js/repeater.js" type="text/javascript"></script>
    <script>
        $(document).ready(function() {
            $('#repeater').createRepeaterZ();
        });
    </script>
</body>

</html>