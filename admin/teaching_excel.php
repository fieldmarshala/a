<?php

include("../inc/connect.php");
include('php/checklogin.php');
require '../assets/plugins/phpspreadsheet/vendor/autoload.php';
$db = new database();
$con = $db->connect();
error_reporting(0);

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

$treatise_type = "teaching";
$search_name = $_GET['search_name'];
$search_surname = $_GET['search_surname'];
$start_year = $_GET['start_year'];
$end_year = $_GET['end_year'];

if ($search_name) {
    $sql_teaching = "SELECT author.*,teaching.* ";
    $sql_teaching .= "FROM author,teaching ";
    $sql_teaching .= "WHERE author.treatise_id = teaching.item_id ";
    $sql_teaching .= "and author.name = '$search_name' ";
    $sql_teaching .= "and author.surname = '$search_surname' ";
    $sql_teaching .= "and treatise_type = 'teaching' ";
    if ($start_year) {
        $sql_teaching .= "and year_make >= '$start_year' ";
    } else if ($end_year) {
        $sql_teaching .= "and year_make <= '$end_year' ";
    } else if ($start_year && $end_year) {
        $sql_teaching .= "and year_make >= '$start_year' and year_make <= '$end_year' ";
    }
} else {
    $sql_teaching = "SELECT * FROM teaching ";
    if ($start_year) {
        $sql_teaching .= "WHERE year_make >= '$start_year' ";
    } else if ($end_year) {
        $sql_teaching .= "WHERE year_make <= '$end_year' ";
    } else if ($start_year && $end_year) {
        $sql_teaching .= "WHERE year_make >= '$start_year' and year_make <= '$end_year' ";
    }
}

$sql_teaching .= "ORDER BY teaching_name";

$query_teaching = $con->query($sql_teaching);

$spreadsheet = new Spreadsheet();
$spreadsheet->getDefaultStyle()->getFont()->setName('TH Sarabun New');
$spreadsheet->getDefaultStyle()->getFont()->setSize(18);
$spreadsheet->getDefaultStyle()->getAlignment()->setHorizontal('center');
$spreadsheet->getActiveSheet()->getStyle('A1:F1')->getFont()->setSize(22);
$spreadsheet->getActiveSheet()->getStyle('A1:F1')->getFont()->setBold('Bold');
$spreadsheet->getActiveSheet()->getStyle('F')->getAlignment()->setHorizontal('left');
for ($col = 'A'; $col != 'G'; $col++) {
    $spreadsheet->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
}
$sheet = $spreadsheet->getActiveSheet();
$sheet->setCellValue('A1', 'เรื่องที่');
$sheet->setCellValue('B1', 'ชื่อเอกสารประกอบการสอน');
$sheet->setCellValue('C1', 'จำนวนหน้า');
$sheet->setCellValue('D1', 'ปีที่แต่ง');
$sheet->setCellValue('E1', 'คณะ');
$sheet->setCellValue('F1', 'รายชื่อผู้แต่ง');

if ($query_teaching->num_rows > 0) {
    $result_row = 1;
    $i = 1;
    while ($result_teaching = $query_teaching->fetch_object()) {

        $item_id = $result_teaching->item_id;
        //fetch author
        $sql_author = "SELECT * FROM author WHERE treatise_type = '$treatise_type' AND treatise_id = '$item_id' ORDER BY author_id";
        $query_author = $con->query($sql_author);
        $num_author = $query_author->num_rows;

        $teaching_name = $result_teaching->teaching_name;
        $year_make = $result_teaching->year_make;
        $page = $result_teaching->page;
        $faculty = $result_teaching->faculty;

        $rowNum = $result_row + 1;

        $sheet->setCellValue('A' . $rowNum, $i);
        $sheet->setCellValue('B' . $rowNum, $teaching_name);
        $sheet->setCellValue('C' . $rowNum, $page);
        $sheet->setCellValue('D' . $rowNum, $year_make);
        $sheet->setCellValue('E' . $rowNum, $faculty);
        $sheet->setCellValue('F' . $rowNum, "จำนวนผู้แต่ง " . $num_author . " คน");

        $ii = 1;
        while ($result_author = $query_author->fetch_object()) {
            $author_name = $result_author->name;
            $author_surname = $result_author->surname;
            $author_name_title = $result_author->name_title;
            $author_academic_rank = $result_author->academic_rank;
            if ($author_name_title == "dr") {
                $name_title_show = "ดร.";
            } else {
                $name_title_show = "";
            }

            if ($author_academic_rank == "pro") {
                $academic_rank_show = "ศ.";
            } else if ($author_academic_rank == "asso") {
                $academic_rank_show = "ร.ศ.";
            } else if ($author_academic_rank == "assis") {
                $academic_rank_show = "ผ.ศ.";
            } else if ($author_academic_rank == "lec") {
                if ($name_title_show == "ดร.") {
                    $academic_rank_show = "อาจารย์ ";
                } else {
                    $academic_rank_show = "อาจารย์ ";
                }
            }
            $author_row = $result_row + 2;
            $sheet->setCellValue('F' . $author_row, "คนที่ " . $ii . " : " . $academic_rank_show . $name_title_show . " " . $author_name . " " . $author_surname);
            $result_row++;
            $ii++;
        }
        $result_row++;
        $i++;
    }
}

$filename = 'เอกสารประกอบการสอน-' . time() . '.xlsx';
// Redirect output to a client's web browser (Xlsx)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.

$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
