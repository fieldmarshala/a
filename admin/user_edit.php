<?php
include("../inc/connect.php");
include('php/checklogin.php');
$db = new database();
$con = $db->connect();

$get_user_id = $_GET['user_id'];
$sql_user = "SELECT * FROM user WHERE user_id = '$get_user_id'";
$query_user = $con->query($sql_user);
$result_user = $query_user->fetch_object();

$work_user = "SELECT * FROM work_detail WHERE user_id = '$get_user_id'";
$query_work_user = $con->query($work_user);
$result_work_user = $query_work_user->fetch_object();

$start_working = DateConvertBase($result_work_user->start_working);
$birthday = DateConvertBase($result_user->birthday);

$strSQL = "SELECT * FROM profile_img WHERE user_id = '$result_user->user_id' ";
$objQuery = $con->query($strSQL);
$result_image = $objQuery->fetch_object();
$image_name = $result_image->image_name;
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon.png">
    <title><?php include("inc/title.php"); ?></title>
    <!-- Bootstrap Core CSS -->
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- page CSS -->
    <link href="../assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/switchery/dist/switchery.min.css" rel="stylesheet" />
    <!--alerts CSS -->
    <link href="../assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="css/colors/blue.css" id="theme" rel="stylesheet">
    <!--  dropzone css -->
    <link rel="stylesheet" href="../assets/plugins/dropzone/min/dropzone.min.css">
    <link href="css/dropzone_custom.css" rel="stylesheet" type="text/css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <?php include("inc/topmenu.php") ?>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <?php include("inc/left_sidebar.php"); ?>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <?php $page_title = "ผู้ใช้งาน"; ?>
            <?php $page_detail = "แก้ไขข้อมูลผู้ใช้งาน" ?>
            <?php $page_title_active = "breadcrumb-item"; ?>
            <?php $page_detail_active = "breadcrumb-item active"; ?>
            <?php include("inc/breadcrumb.php"); ?>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-outline-info">
                            <div class="card-header">
                                <h4 class="m-b-0 text-white"><?= $page_detail; ?></h4>
                            </div>
                            <div class="card-body">
                                <div class="form-body">
                                    <h3 class="card-title"><a href="user.php" class="btn waves-effect waves-light btn-secondary"><i class="fa fa-lg mdi mdi-chevron-left"></i>กลับหน้าผู้ใช้งาน</a></h3>
                                    <form id="form_user" name="form_user" method="post" action="">
                                        <!--/row-->
                                        <div class="row p-t-20">
                                            <div class="col-md-12">
                                                <div id="form_user_academic_rank" class="form-group">
                                                    <?php
                                                    $academic_rank = $result_user->academic_rank;
                                                    if ($academic_rank == "pro") {
                                                        $academic_rank_show = "ศ.";
                                                    } else if ($academic_rank == "asso") {
                                                        $academic_rank_show = "ร.ศ.";
                                                    } else if ($academic_rank == "assis") {
                                                        $academic_rank_show = "ผ.ศ.";
                                                    } else if ($academic_rank == "lec") {
                                                        $academic_rank_show = "อาจารย์";
                                                    }
                                                    ?>
                                                    <label id="label_user_academic_rank" class="control-label">ตำแหน่งทางวิชาการ</label>
                                                    <select id="user_academic_rank" name="user_academic_rank" class="form-control select2">
                                                        <option value="<?= $result_user->academic_rank; ?>" selected><?= $academic_rank_show; ?></option>
                                                        <option value="">------------ เลือก ------------</option>
                                                        <option value="pro">ศ.</option>
                                                        <option value="asso">ร.ศ.</option>
                                                        <option value="assis">ผ.ศ.</option>
                                                        <option value="lec">อาจารย์</option>
                                                    </select>
                                                </div>
                                                <div id="form_user_status" class="form-group">
                                                    <?php
                                                    $name_title = $result_user->name_title;
                                                    if ($name_title == "dr") {
                                                        $name_title_show = "ดร.";
                                                    } else if ($name_title == "mr") {
                                                        $name_title_show = "นาย";
                                                    } else if ($name_title == "ms") {
                                                        $name_title_show = "นางสาว";
                                                    }
                                                    ?>
                                                    <label id="label_user_name_title" class="control-label">คำนำหน้าชื่อ</label>
                                                    <select id="user_name_title" name="user_name_title" class="form-control select2">
                                                        <option value="<?= $result_user->name_title; ?>" selected><?= $name_title_show; ?></option>
                                                        <option value="">------------ เลือก ------------</option>
                                                        <option value="mr">นาย</option>
                                                        <option value="ms">นางสาว</option>
                                                        <option value="dr">ดร.</option>
                                                    </select>
                                                </div>
                                                <div id="form_user_name" class="form-group">
                                                    <label id="label_user_name" class="control-label">ชื่อ</label>
                                                    <input type="hidden" id="user_id" name="user_id" value="<?= $result_user->user_id; ?>">
                                                    <input type="text" id="user_name" name="user_name" class="form-control" value="<?= $result_user->name; ?>">
                                                </div>
                                                <div id="form_user_surname" class="form-group">
                                                    <label id="label_user_surname" class="control-label">นามสกุล</label>
                                                    <input type="text" id="user_surname" name="user_surname" class="form-control" value="<?= $result_user->surname; ?>">
                                                </div>
                                                <div id="form_user_birthday" class="form-group">
                                                    <label id="label_user_birthday" class="control-label">วัน/เดือน/ปีเกิด</label>
                                                    <input type="text" id="birthday" data-date-language="th-th" name="user_birthday" class="form-control" value="<?= $birthday; ?>">
                                                </div>
                                                <div id="form_user_education" class="form-group">
                                                    <label id="label_user_education" class="control-label">การศึกษา</label>
                                                    <textarea id="user_education" name="user_education" class="form-control" rows="5"><?= $result_user->education; ?></textarea>
                                                </div>
                                                <div id="form_user_tel" class="form-group">
                                                    <label id="label_user_tel" class="control-label">เบอร์โทรศัพท์</label>
                                                    <input type="text" id="user_tel" name="user_tel" class="form-control" value="<?= $result_user->phone; ?>">
                                                </div>
                                                <div id="form_user_email" class="form-group">
                                                    <label id="label_user_email" class="control-label">อีเมล</label>
                                                    <input type="text" id="user_email" name="user_email" class="form-control" value="<?= $result_user->email; ?>">
                                                </div>
                                                <div id="form_user_local_address" class="form-group">
                                                    <label id="label_user_local_address" class="control-label">ภูมิลำเนา</label>
                                                    <textarea id="user_local_address" name="user_local_address" class="form-control" rows="5"><?= $result_user->local_address; ?></textarea>
                                                </div>
                                                <div id="form_user_local_province" class="form-group">
                                                    <?php
                                                    $local_province_id = $result_user->local_province;
                                                    $sql_local_province = "SELECT * FROM province WHERE province_id = '$local_province_id'";
                                                    $q_local_province = $con->query($sql_local_province);
                                                    $r_local_province = $q_local_province->fetch_object();
                                                    ?>
                                                    <label id="label_user_local_province" class="control-label">ภูมิลำเนา(จังหวัด)</label>
                                                    <select id="user_local_province" name="user_local_province" class="form-control select2">
                                                        <option value="<?= $local_province_id; ?>" selected><?= $r_local_province->province_name; ?></option>
                                                        <option value="">------------ เลือก ------------</option>
                                                    </select>
                                                </div>
                                                <div id="form_user_address" class="form-group">
                                                    <label id="label_user_address" class="control-label">ที่อยู่ปัจจุบัน</label>
                                                    <textarea id="user_address" name="user_address" class="form-control" rows="5"><?= $result_user->address; ?></textarea>
                                                </div>
                                                <div id="form_user_province" class="form-group">
                                                    <?php
                                                    $province_id = $result_user->province;
                                                    $sql_province = "SELECT * FROM province WHERE province_id = '$province_id'";
                                                    $q_province = $con->query($sql_province);
                                                    $r_province = $q_province->fetch_object();
                                                    ?>
                                                    <label id="label_user_province" class="control-label">จังหวัดที่อยู่ปัจจุบัน</label>
                                                    <select id="user_province" name="user_province" class="form-control select2">
                                                        <option value="<?= $province_id; ?>" selected><?= $r_province->province_name; ?></option>
                                                        <option value="">------------ เลือก ------------</option>
                                                    </select>
                                                </div>
                                                <div id="form_user_id_card" class="form-group">
                                                    <label id="label_user_id_card" class="control-label">หมายเลขบัตรประชาชน</label>
                                                    <input type="text" id="user_id_card" name="user_id_card" class="form-control" value="<?= $result_user->id_card; ?>">
                                                </div>
                                                <h3><u>ข้อมูลการทำงาน</u></h3>
                                                <div id="form_user_start_working" class="form-group">
                                                    <label id="label_user_start_working" class="control-label">วันที่บรรจุ</label>
                                                    <input type="text" id="start_working" data-date-language="th-th" name="user_start_working" class="form-control" value="<?= $start_working; ?>">
                                                </div>
                                                <div id="form_user_work_type" class="form-group">
                                                    <label id="label_user_work_type" class="control-label">ประเภทสายงาน</label>
                                                    <input type="text" id="user_work_type" name="user_work_type" class="form-control" value="<?= $result_work_user->work_type; ?>">
                                                </div>
                                                <div id="form_user_budget_type" class="form-group">
                                                    <label id="label_user_budget_type" class="control-label">ประเภทงบประมาณที่จัดจ้าง</label>
                                                    <input type="text" id="user_budget_type" name="user_budget_type" class="form-control" value="<?= $result_work_user->budget_type; ?>">
                                                </div>
                                                <div id="form_user_type" class="form-group">
                                                    <label id="label_user_type" class="control-label">ประเภทบุคลากร</label>
                                                    <input type="text" id="user_type" name="user_type" class="form-control" value="<?= $result_work_user->user_type; ?>">
                                                </div>
                                                <div id="form_user_username" class="form-group">
                                                    <label id="label_user_username" class="control-label">username</label>
                                                    <input type="text" id="user_username" name="user_username" class="form-control" value="<?= $result_user->user_username; ?>" readonly>
                                                </div>
                                                <div id="form_user_password" class="form-group">
                                                    <label id="label_user_password" class="control-label">รหัสผ่าน</label>
                                                    <input type="password" id="user_password" name="user_password" class="form-control" value="<?= $result_user->user_password; ?>">
                                                </div>
                                                <div id="form_user_cpassword" class="form-group">
                                                    <label id="label_user_cpassword" class="control-label">ยืนยันรหัสผ่าน</label>
                                                    <input type="password" id="user_cpassword" name="user_cpassword" class="form-control" value="<?= $result_user->user_password; ?>">
                                                </div>
                                                <div id="form_user_status" class="form-group">
                                                    <?php
                                                    $user_status = $result_user->user_status;
                                                    if ($user_status == "admin") {
                                                        $user_status_show = "ผู้ดูแลระบบ";
                                                    } else {
                                                        $user_status_show = "ผู้ใช้งาน";
                                                    }
                                                    ?>
                                                    <label id="label_user_status" class="control-label">สถานะ</label>
                                                    <select id="user_status" name="user_status" class="form-control select2">
                                                        <option value="<?= $result_user->user_status; ?>" selected><?= $user_status_show; ?></option>
                                                        <option value="">------------ เลือก ------------</option>
                                                        <option value="admin">ผู้ดูแลระบบ</option>
                                                        <option value="user">ผู้ใช้งาน</option>
                                                    </select>
                                                </div>
                                                <label for="pic_profile">รูปภาพเดิม</label>
                                                <div>
                                                    <img src="../images/profiles/<?= $image_name; ?>" width="100px">
                                                    <input type="hidden" name="old_pic" value="<?=$image_name;?>">  <!-- ส่งรูปเก่าไปด้วย -->
                                                </div><br>
                                                <label id="label_user_img" class="control-label">อัพโหลดรูปโปรไฟล์</label>
                                                <div id="form_user_img" class="form-group">
                                                    <div class="dropzone" id="drop"></div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                    </form>
                                </div> <!-- form body -->
                                <div class="form-actions">
                                    <button type="button" id="edit" class="btn btn-success col-md-1">บันทึก</button>
                                    <button onclick="history.go(-1);" class="btn btn-inverse col-md-1">ยกเลิก</button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <?php include("inc/right_sidebar.php"); ?>
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"><?php include("inc/footer.php"); ?></footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="../assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../assets/plugins/bootstrap/js/popper.min.js"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="../assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <script src="../assets/plugins/switchery/dist/switchery.min.js"></script>
    <script src="../assets/plugins/select2/dist/js/select2.full.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
    <script src="../assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../assets/plugins/multiselect/js/jquery.multi-select.js"></script>
    <!-- Sweet-Alert  -->
    <script src="../assets/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="../assets/plugins/sweetalert/jquery.sweet-alert.custom.js"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="../assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="../assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- JS Datepicker-->
    <script src="js/bootstrap-datepicker/js/bootstrap-datepicker-thai.js"></script>
    <script src="js/bootstrap-datepicker/locales/bootstrap-datepicker.th.js"></script>
    <!-- Dropzone -->
    <script src="../assets/plugins/dropzone/min/dropzone.min.js"></script>
    <!-- custom js -->
    <script src="js/user.js"></script>
    <script src="js/login.js"></script>
    <script src="js/dropzone_custom.js"></script>
    <script src="js/conference.js"></script>
    <script>
        $(document).ready(function() {
            $('#birthday').datepicker({
                format: "dd/mm/yyyy",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true
            }).datepicker();

            $('#start_working').datepicker({
                format: "dd/mm/yyyy",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true
            }).datepicker();
        });
    </script>


</body>

</html>