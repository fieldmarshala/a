<?php
include("../inc/connect.php");
include('php/checklogin.php');
$db = new database();
$con = $db->connect();
/*
$treatise_type = "research";
$wh = "WHERE item_id = '1'";
$sql_res = "SELECT * FROM $treatise_type ";
$query_research = $con->query($sql_res);
*/
$treatise_type = "research";
if ($search_name) {
  $sql_research = "SELECT author.*,research.* ";
  $sql_research .= "FROM author,research ";
  $sql_research .= "WHERE author.treatise_id = research.item_id ";
  $sql_research .= "and author.name = '$search_name' ";
  $sql_research .= "and treatise_type = 'research' ";
  if ($research_status) {
    if ($research_status == "overdue") {
      $sql_research .= "and (research_status = 'start' or research_status = 'processing') ";
      $sql_research .= "and research_processdate < '$today' ";
    } else {
      $sql_research .= "and research_status = '$research_status' ";
    }
  }
} else {
  $sql_research = "SELECT * FROM research ";
  if ($research_status) {
    if ($research_status == "overdue") {
      $sql_research .= "WHERE (research_status = 'start' or research_status = 'processing') ";
      $sql_research .= "and research_processdate < '$today' ";
    } else {
      $sql_research .= "WHERE research_status = '$research_status' ";
    }
  }
}

$sql_research .= "ORDER BY research_name";

$query_research = $con->query($sql_research);

//fetch research
$num_research = $query_research->num_rows;
echo "Num Rows = " . $num_research . "<br>";

/*
echo "<pre>";
print_r($result_researchearch);
echo "</pre>";
*/

/*
header("Content-Type: application/vnd.ms-excel");
header('Content-Disposition: attachment; filename="myexcel.xls"');
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Transfer-Encoding: binary");
header("Content-Length: " . filesize("myexcel.xls"));
*/
@readfile($filename);


?>

<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">

<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body>
  <?php
  $wwa = $_GET['wwa'];
  ?>
  <table border="1">
    <tr>
      <td align="center">Research ID</td>
      <td align="center">ชื่องานวิจัย</td>
      <td align="center">สถานะงานวิจัย</td>
      <td>WWA</td>
      <td align="center">รายชื่อผู้แต่ง</td>
    </tr>
    <?php
    while ($result_research = $query_research->fetch_object()) {
      $item_id = $result_research->item_id;
      //fetch author
      $sql_author = "SELECT * FROM author WHERE treatise_type = '$treatise_type' AND treatise_id = '$item_id' ORDER BY author_id";
      $query_author = $con->query($sql_author);

      $research_name = $result_research->research_name;
      $research_status = $result_research->research_status;
      $research_budget = $result_research->research_budget;

      if ($research_status == "complete") {
        $research_status_show = "เสร็จสิ้น";
      } else if ($research_status == "processing") {
        $research_status_show = "กำลังดำเนินการ";
      } else if ($research_status == "start") {
        $research_status_show = "เริ่มโครงร่างวิจัย";
      } else if ($research_status == "overdue") {
        $research_status_show = "เกินกำหนดระยะเวลา";
      }

      $i = 1;
      echo '<tr>
				<td align="center">' . $item_id . '</td>
        <td>' . $research_name . '</td>
        <td>' . $research_status_show . '</td>
        <td>' . $wwa . '</td>';
      while ($result_author = $query_author->fetch_object()) {
        $author_name = $result_author->name;
        $author_surname = $result_author->surname;
        $author_name_title = $result_author->name_title;
        $author_academic_rank = $result_author->academic_rank;

        if ($author_name_title == "dr") {
          $name_title_show = "ดร.";
        } else {
          $name_title_show = "";
        }

        if ($author_academic_rank == "pro") {
          $academic_rank_show = "ศ.";
        } else if ($author_academic_rank == "asso") {
          $academic_rank_show = "ร.ศ.";
        } else if ($author_academic_rank == "assis") {
          $academic_rank_show = "ผ.ศ.";
        } else if ($author_academic_rank == "lec") {
          $academic_rank_show = "อาจารย์";
        }
        echo '<td>' . "ผู้แต่งคนที่ " . $i . " : " . $academic_rank_show, $name_title_show . " " . $author_name . " " . $author_surname . '</td>';
        $i++;
      }
      '</tr>';
    }
    ?>
  </table>
</body>

</html>