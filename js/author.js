var author_count = 1;

$(function () {
    $(document).on('click', '.btn-add', function (e) {
        e.preventDefault();
        if (author_count < 8) {
            author_count++; var controlForm = $('.author_form:first'),
                currentEntry = $(this).parents('.entryZ:first'),
                newEntry = $(currentEntry.clone()).appendTo(controlForm);

            newEntry.find('input').val('');
            controlForm.find('.entryZ:not(:last) .btn-add')
                .removeClass('btn-add').addClass('btn-remove')
                .removeClass('btn-success').addClass('btn-danger')
                .html('<span class="mdi mdi-account-remove col-md-12"> ลบรายชื่อผู้วิจัย</span>');
        }
    }).on('click', '.btn-remove', function (e) {
        $(this).parents('.entryZ:first').remove();
        author_count--;
        e.preventDefault();
        return false;
    });
});
