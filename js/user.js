$(function() {

	// show province register page
	//ดึงข้อมูล เอกสาร จากไฟล์ get_datalist.php
	$.ajax({
		url:"php/get_datalist.php",
		dataType: "json", //กำหนดให้มีรูปแบบเป็น Json
		data:{show_province:'show_province'}, //ส่งค่าตัวแปร show_province ไปที่ get_province.php เพื่อดึงข้อมูล province
			success:function(data){                        
			    //วนลูปแสดงข้อมูล ที่ได้จาก ตัวแปร data
			    $.each(data, function( index, value ) {
			        //แทรก Elements ใน id med_docType  ด้วยคำสั่ง append
			          $("#user_province").append("<option value='"+ value.province_id +"'> " + value.province_name + "</option>");
								$("#user_local_province").append("<option value='"+ value.province_id +"'> " + value.province_name + "</option>");
							});
			}
	}); // show province

	  $("#user_name").keydown(function(event) {
	    $("#form_user_name").removeClass();
	    $("#form_user_name").addClass("form-group");
	    $("#label_user_name").removeClass();
	    $("#label_user_name").addClass("control-label");
	    $("#user_name").removeClass();
	    $("#user_name").addClass("form-control");
	  });

	  $("#user_email").keydown(function(event) {
	    $("#form_user_email").removeClass();
	    $("#form_user_email").addClass("form-group");
	    $("#label_user_email").removeClass();
	    $("#label_user_email").addClass("control-label");
	    $("#user_email").removeClass();
	    $("#user_email").addClass("form-control");
	  });

	  $("#user_username").keydown(function(event) {
	    $("#form_user_username").removeClass();
	    $("#form_user_username").addClass("form-group");
	    $("#label_user_username").removeClass();
	    $("#label_user_username").addClass("control-label");
	    $("#user_username").removeClass();
	    $("#user_username").addClass("form-control");
	  });

	  $("#user_password").keydown(function(event) {
	    $("#form_user_password").removeClass();
	    $("#form_user_password").addClass("form-group");
	    $("#label_user_password").removeClass();
	    $("#label_user_password").addClass("control-label");
	    $("#user_password").removeClass();
	    $("#user_password").addClass("form-control");
	  });

	  $("#user_cpassword").keydown(function(event) {
	    $("#form_user_cpassword").removeClass();
	    $("#form_user_cpassword").addClass("form-group");
	    $("#label_user_cpassword").removeClass();
	    $("#label_user_cpassword").addClass("control-label");
	    $("#user_cpassword").removeClass();
	    $("#user_cpassword").addClass("form-control");
	  });
  
});

//ลบผู้วิจัย
function delete_author(author_id_1) {
  var treatise_id = $('#treatise_id').val();
  swal({
    title: "คุณต้องการลบผู้วิจัย?",
    text: "",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#26dad2",
    confirmButtonText: "ตกลง",
    cancelButtonText: "ยกเลิก",
    closeOnConfirm: false,
    closeOnCancel: true
  }, function (isConfirm) {
    if (isConfirm) {
      $.ajax({
        url: "php/user_process.php",
        type: "post", //กำหนดให้มีรูปแบบเป็น Json
        data: {
          delete_author: 'delete_author',
          author_id_2: author_id_1
        },
        success: function (data) {
          if (data == "yes") {
            swal({
              title: "ลบผู้วิจัยเรียบร้อย",
              text: "",
              type: "success",
              confirmButtonColor: "#26dad2",
              confirmButtonText: "OK"
            }, function () {
              window.location.replace("research_edit.php?research_id=" + treatise_id);
            }); // swal
          } // if data == yes
        } // success data
      }); // ajax
    }// isConfirm
    else {
      return false;
    }
  });
}; // #delete author button

//เพิ่มรายชื่อผู้วิจัยจากฟอร์มย่อย research_edit.php
$('#add_author').click(function () {
  var treatise_id = $('#treatise_id').val();
  if ($('#add_name').val() == '') {
		$("#label_author_add").addClass("text-danger");
    $('#add_name').focus();
    swal({
      title: "กรุณาใส่ชื่อผู้วิจัย",
      text: "",
      type: "error",
      showCancelButton: false,
      confirmButtonColor: "#26dad2",
      confirmButtonText: 'OK!',
    });
    return false;
  } 
  else if ($('#add_surname').val() == '') {
		$("#label_author_add").addClass("text-danger");
    $('#add_surname').focus();
    swal({
      title: "กรุณาใส่นามสกุลผู้วิจัย",
      text: "",
      type: "error",
      showCancelButton: false,
      confirmButtonColor: "#26dad2",
      confirmButtonText: 'OK!',
    });
    return false;
  }
  else if ($('#add_academic_rank').val() == '') {
		$("#label_author_add").addClass("text-danger");
    $('#add_academic_rank').focus();
    swal({
      title: "กรุณาเลือกตำแหน่งทางวิชาการของผู้วิจัย",
      text: "",
      type: "error",
      showCancelButton: false,
      confirmButtonColor: "#26dad2",
      confirmButtonText: 'OK!',
    });
    return false;
  }
  else if ($('#add_name_title').val() == '') {
		$("#label_author_add").addClass("text-danger");
    $('#add_name_title').focus();
    swal({
      title: "กรุณาเลือกคำนำหน้าชื่อของผู้วิจัย",
      text: "",
      type: "error",
      showCancelButton: false,
      confirmButtonColor: "#26dad2",
      confirmButtonText: 'OK!',
    });
    return false;
  }
  else {
    swal({
      title: "คุณต้องการเพิ่มผู้วิจัย?",
      text: "",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#26dad2",
      confirmButtonText: "บันทึก",
      cancelButtonText: "ยกเลิก",
      closeOnConfirm: false,
      closeOnCancel: true
    }, function (isConfirm) {
      if (isConfirm) {
        var form_user = $("#form_research");
        $.ajax({
          url: "php/user_process.php",
          type: "post", //กำหนดให้มีรูปแบบเป็น post
          data: form_user.serialize() + "&add_author_from_edit=add_author_from_edit",
          success: function (data) {
            if (data == "yes") {
              swal({
                title: "บันทึกข้อมูลเรียบร้อย",
                text: "",
                type: "success",
                confirmButtonColor: "#26dad2",
                confirmButtonText: "OK"
              }, function () {
                window.location.replace("research_edit.php?research_id=" + treatise_id);
              }); // swal
            } // if data == yes
          } // success data
        }); // ajax
      }// isConfirm
      else {
        return false;
      }
    });
  }
}); // #add_author button เพิ่มผู้วิจัยหน้าแก้ไขงานวิจัย