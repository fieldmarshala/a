$(function () {

  $('#save_research').click(function () {

    var emptyName = false;

    // เช็คค่าว่างของชื่อผู้วิจัย
    if ($.each($('.name'), function () {
      if ($(this).val() == '') {
        emptyName = true;
      }
    })
    )

      if (emptyName) {
        $.each($('.name'), function () {
          if ($(this).val() == '') {
            $(this).focus();
            swal({
              title: "กรุณาใส่ชื่อรายชื่อผู้วิจัย",
              text: "",
              type: "error",
              showCancelButton: false,
              confirmButtonColor: "#26dad2",
              confirmButtonText: 'OK!',
            });
            return false;
          }
        });
      }
      else if ($('#research_name').val() == '') {
        $('#research_name').focus();
        swal({
          title: "กรุณาใส่ชื่องานวิจัย",
          text: "",
          type: "error",
          showCancelButton: false,
          confirmButtonColor: "#26dad2",
          confirmButtonText: 'OK!',
        });
        return false;
      }
      else if ($('#research_timeline').val() == '') {
        $('#research_timeline').focus();
        swal({
          title: "กรุณาใส่ช่วงเวลาที่จัดทำวิจัย",
          text: "",
          type: "error",
          showCancelButton: false,
          confirmButtonColor: "#26dad2",
          confirmButtonText: 'OK!',
        });
        return false;
      }
      else if ($('#research_status').val() == '') {
        $('#research_status').focus();
        swal({
          title: "กรุณาเลือกสถานะงานวิจัย",
          text: "",
          type: "error",
          showCancelButton: false,
          confirmButtonColor: "#26dad2",
          confirmButtonText: 'OK!',
        });
        return false;
      }
      else if ((($('#research_status').val() == 'start') || ($('#research_status').val() == 'processing')) && ($('#research_startdate').val() == '')) {
        $('#research_startdate').focus();
        swal({
          title: "กรุณาเลือกวันที่เริ่มต้นทำงานวิจัย",
          text: "",
          type: "error",
          showCancelButton: false,
          confirmButtonColor: "#26dad2",
          confirmButtonText: 'OK!',
        });
        return false;
      }
      else if ((($('#research_status').val() == 'start') || ($('#research_status').val() == 'processing')) && ($('#research_processdate').val() == '')) {
        $('#research_processdate').focus();
        swal({
          title: "กรุณาเลือกระยะเวลาดำเนินการวิจัย",
          text: "",
          type: "error",
          showCancelButton: false,
          confirmButtonColor: "#26dad2",
          confirmButtonText: 'OK!',
        });
        return false;
      }
      else if (($('#research_status').val() == 'complete') && ($('#research_startdate').val() == '')) {
        $('#research_startdate').focus();
        swal({
          title: "กรุณาเลือกวันที่เริ่มต้นทำงานวิจัย",
          text: "",
          type: "error",
          showCancelButton: false,
          confirmButtonColor: "#26dad2",
          confirmButtonText: 'OK!',
        });
        return false;
      }
      else if (($('#research_status').val() == 'complete') && ($('#research_enddate').val() == '')) {
        $('#research_enddate').focus();
        swal({
          title: "กรุณาเลือกวันที่งานวิจัยเสร็จสิ้น",
          text: "",
          type: "error",
          showCancelButton: false,
          confirmButtonColor: "#26dad2",
          confirmButtonText: 'OK!',
        });
        return false;
      }
      else if (($('#research_status').val() == 'complete') && ($('#research_processdate').val() == '')) {
        $('#research_processdate').focus();
        swal({
          title: "กรุณาเลือกระยะเวลาดำเนินการวิจัย",
          text: "",
          type: "error",
          showCancelButton: false,
          confirmButtonColor: "#26dad2",
          confirmButtonText: 'OK!',
        });
        return false;
      }
      else if ($('#research_budget_from').val() == '') {
        $('#research_budget_from').focus();
        swal({
          title: "กรุณาใส่แหล่งเงินทุนของงานวิจัย",
          text: "",
          type: "error",
          showCancelButton: false,
          confirmButtonColor: "#26dad2",
          confirmButtonText: 'OK!',
        });
        return false;
      }
      else if ($('#research_budget_year').val() == '') {
        $('#research_budget_year').focus();
        swal({
          title: "กรุณาใส่ปีงบประมาณที่ขอทุนวิจัย",
          text: "",
          type: "error",
          showCancelButton: false,
          confirmButtonColor: "#26dad2",
          confirmButtonText: 'OK!',
        });
        return false;
      }
      else if ($('#research_budget').val() == '') {
        $('#research_budget').focus();
        swal({
          title: "กรุณาใส่จำนวนงบประมาณที่ขอทุนวิจัย",
          text: "",
          type: "error",
          showCancelButton: false,
          confirmButtonColor: "#26dad2",
          confirmButtonText: 'OK!',
        });
        return false;
      }
      else {
        swal({
          title: "คุณต้องการบันทึกข้อมูล?",
          text: "",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#26dad2",
          confirmButtonText: "บันทึก",
          cancelButtonText: "ยกเลิก",
          closeOnConfirm: false,
          closeOnCancel: true
        }, function (isConfirm) {
          if (isConfirm) {
            var form_research = $("#form_research");
            $.ajax({
              url: "php/research_process.php",
              type: "post", //กำหนดให้มีรูปแบบเป็น post
              data: form_research.serialize() + "&add_research=add_research",
              success: function (data) {
                if (data == "yes") {
                  swal({
                    title: "บันทึกข้อมูลเรียบร้อย",
                    text: "",
                    type: "success",
                    confirmButtonColor: "#26dad2",
                    confirmButtonText: "OK"
                  }, function () {
                    window.location.replace("report.php");
                  }); // swal
                }
                else {
                  console.log(data);
                }
              } // success data
            }); // ajax
          }// isConfirm
          else {
            return false;
          }
        });
      }
  }); // #save_research button

  $('#edit_res').click(function () {
    var treatise_id = $('#treatise_id').val();
    swal({
      title: "คุณต้องการบันทึกข้อมูล?",
      text: "",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#26dad2",
      confirmButtonText: "บันทึก",
      cancelButtonText: "ยกเลิก",
      closeOnConfirm: false,
      closeOnCancel: true
    }, function (isConfirm) {
      if (isConfirm) {
        var form_research = $("#form_research");
        $.ajax({
          url: "php/research_process.php",
          type: "post", //กำหนดให้มีรูปแบบเป็น post
          data: form_research.serialize() + "&edit_research=edit_research",
          success: function (data) {
            if (data == "yes") {
              swal({
                title: "บันทึกข้อมูลเรียบร้อย",
                text: "",
                type: "success",
                confirmButtonColor: "#26dad2",
                confirmButtonText: "OK"
              }, function () {
                window.location.replace("report.php");
                //window.location.replace("full_detail_research.php?research_id=" + treatise_id);
              }); // swal
            } // if data == yes
            else {
              console.log(data);
            }
          } // success data
        }); // ajax
      }// isConfirm
      else {
        return false;
      }
    });
  }); // #edit_res button

  $(".select2").select2({
    minimumResultsForSearch: -1 // ปิด Textbox ของ select2 ตอนเลือก listbox
  });

  if ($('#research_status').val() == 'start') {
    $("#research_enddate").attr('disabled', true);
    $("#research_enddate").val('');
  } else if ($('#research_status').val() == 'processing') {
    $("#research_enddate").attr('disabled', true);
    $("#research_enddate").val('');
  };

  $('#research_status').on('change', function () {
    if (this.value == 'start') {
      $("#research_startdate").attr('disabled', false);
      $("#research_processdate").attr('disabled', false);
      $("#research_enddate").attr('disabled', true);
      $("#research_enddate").val('');
    } else if (this.value == 'processing') {
      $("#research_startdate").attr('disabled', false);
      $("#research_processdate").attr('disabled', false);
      $("#research_enddate").attr('disabled', true);
      $("#research_enddate").val('');
    } else if (this.value == 'complete') {
      $("#research_startdate").attr('disabled', false);
      $("#research_enddate").attr('disabled', false);
      $("#research_processdate").attr('disabled', false);
    } else {
      $("#research_startdate").attr('disabled', true);
      $("#research_enddate").attr('disabled', true);
      $("#research_processdate").attr('disabled', true);
      $("#research_startdate").val('');
      $("#research_enddate").val('');
      $("#research_processdate").val('');
    }
  });

  $('#research_startdate').datepicker({
    format: "dd/mm/yyyy",
    keyboardNavigation: false,
    forceParse: false,
    calendarWeeks: true,
    autoclose: true
  }).datepicker();

  $('#research_enddate').datepicker({
    format: "dd/mm/yyyy",
    keyboardNavigation: false,
    forceParse: false,
    calendarWeeks: true,
    autoclose: true
  }).datepicker();

  $('#research_processdate').datepicker({
    format: "dd/mm/yyyy",
    keyboardNavigation: false,
    forceParse: false,
    calendarWeeks: true,
    autoclose: true
  }).datepicker();
});

function delete_research(res_id_1, treatise_type_1) {
  swal({
    title: "คุณต้องการลบข้อมูล?",
    text: "",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#26dad2",
    confirmButtonText: "ตกลง",
    cancelButtonText: "ยกเลิก",
    closeOnConfirm: false,
    closeOnCancel: true
  }, function (isConfirm) {
    if (isConfirm) {
      $.ajax({
        url: "php/research_process.php",
        type: "post", //กำหนดให้มีรูปแบบเป็น Json
        data: {
          delete_research: 'delete_research',
          res_id_2: res_id_1,
          treatise_type_2: treatise_type_1
        },
        success: function (data) {
          if (data == "yes") {
            swal({
              title: "ลบข้อมูลเรียบร้อย",
              text: "",
              type: "success",
              confirmButtonColor: "#26dad2",
              confirmButtonText: "OK"
            }, function () {
              window.location.replace("report.php");
            }); // swal
          } // if data == yes
        } // success data
      }); // ajax
    }// isConfirm
    else {
      return false;
    }
  });
}; // #delete button
