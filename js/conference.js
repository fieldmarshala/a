$(function () {
  $('#repeater').createRepeaterZ();

  $('#showdate').datepicker({
    format: "dd/mm/yyyy",
    todayHighlight: true,
    keyboardNavigation: false,
    forceParse: false,
    calendarWeeks: true,
    autoclose: true
  }).datepicker();

  $("#grade").select2({
    minimumResultsForSearch: -1 // ปิด Textbox ของ select2 ตอนเลือก listbox
  });
});


function delete_research(res_id_1, treatise_type_1) {
  swal({
    title: "คุณต้องการลบข้อมูล?",
    text: "",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#26dad2",
    confirmButtonText: "ตกลง",
    cancelButtonText: "ยกเลิก",
    closeOnConfirm: false,
    closeOnCancel: true
  }, function (isConfirm) {
    if (isConfirm) {
      $.ajax({
        url: "php/research_process.php",
        type: "post", //กำหนดให้มีรูปแบบเป็น Json
        data: {
          delete_research: 'delete_research',
          res_id_2: res_id_1,
          treatise_type_2: treatise_type_1
        },
        success: function (data) {
          if (data == "yes") {
            swal({
              title: "ลบข้อมูลเรียบร้อย",
              text: "",
              type: "success",
              confirmButtonColor: "#26dad2",
              confirmButtonText: "OK"
            }, function () {
              window.location.replace(treatise_type_1 + ".php");
            }); // swal
          } // if data == yes
        } // success data
      }); // ajax
    }// isConfirm
    else {
      return false;
    }
  });
}; // #delete button
