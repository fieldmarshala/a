Dropzone.autoDiscover = false;

var myDropzone = new Dropzone("#drop", {
    url: "admin/php/upload_picture.php",
    maxFiles: "1",
    dictMaxFilesExceeded: "สามารถเลือกได้เพียงรูปเดียวเท่านั้น",
    addRemoveLinks: true,    //เพิ่มการลบรูปที่เลือก (ไม่ได้ลบไฟล์)
    autoProcessQueue: false, //ปิด auto upload
    dictDefaultMessage: "เพิ่มรูปโปรไฟล์",   //เปลี่ยนข้อความ
    dictCancelUpload: "<i class='mdi mdi-close-circle'></i>", //เปลี่ยนข้อความเป็น Icon
    dictRemoveFile: "<i class='mdi mdi-close-circle'></i>",  //เปลี่ยนข้อความเป็น Icon
    acceptedFiles: "image/*",
    dictInvalidFileType: "โปรดเลือกไฟล์รูปภาพเท่านั้น",
    maxFilesize: "2",
    dictFileTooBig: "ไฟล์ใหญ่เกินไป โปรดเลือกไฟล์ขนาดไม่เกิน 2 MB",
    //เปลี่ยนชื่อไฟล์
    renameFile: function (file) {
        let num_ran = Math.floor((Math.random() * 999999999) + 1);   //create random number
        let ext = file.name.split('.').pop();   //get สกุลไฟล์
        let newName = num_ran + '.' + ext;  //create new file with new name
        return newName;
    },
    //เอาชื่อไฟล์ที่ตั้งใหม่ออกมา
    init: function () {
        this.on("addedfile", function (file) {
            file.previewElement.appendChild(
                Dropzone.createElement(`<input type="hidden" name="img_name" value="${file.upload.filename}">`)
            )
        }),
            //เมื่อ error จะแจ้งเตือนตามเรื่องต่างๆ
            this.on("error", function (file, message) {
                swal({
                    title: message,
                    text: "",
                    type: "error",
                    confirmButtonColor: "#26dad2",
                    confirmButtonText: "OK"
                  });
                this.removeFile(file);
            });
    },
});

/*//log response ที่ได้ออกมาดู
myDropzone.on("success", function(file){
    console.log(file);
    
    let res = JSON.parse(file.xhr.response)
    console.log(res);
    
})*/

/*//ปุ่มเอาไว้ test อยู่ที่ test_dropzone.php
$('#gogo').click(function () {
    myDropzone.processQueue()
})*/

$('#edit').click(function () {
    if ($('#user_name').val() == '') {
        $("#form_user_name").addClass("form-group has-danger");
        $("#label_user_name").addClass("text-danger");
        $("#user_name").addClass("form-control form-control-danger");
        $('#user_name').focus();
        swal({
            title: "กรุณาใส่ชื่อ",
            text: "",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#26dad2",
            confirmButtonText: 'OK!',
        });
        return false;
    }
    else if ($('#user_surname').val() == '') {
        $("#form_user_surname").addClass("form-group has-danger");
        $("#label_user_surname").addClass("text-danger");
        $("#user_surname").addClass("form-control form-control-danger");
        $('#user_surname').focus();
        swal({
            title: "กรุณาใส่นามสกุล",
            text: "",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#26dad2",
            confirmButtonText: 'OK!',
        });
        return false;
    }
    else if ($('#user_academic_rank').val() == '') {
        $("#form_user_academic_rank").addClass("form-group has-danger");
        $("#label_user_academic_rank").addClass("text-danger");
        $("#user_academic_rank").addClass("form-control form-control-danger");
        $('#user_academic_rank').focus();
        swal({
            title: "กรุณาเลือกตำแหน่งทางวิชาการ",
            text: "",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#26dad2",
            confirmButtonText: 'OK!',
        });
        return false;
    }
    else if ($('#user_name_title').val() == '') {
        $("#form_user_name_title").addClass("form-group has-danger");
        $("#label_user_name_title").addClass("text-danger");
        $("#user_name_title").addClass("form-control form-control-danger");
        $('#user_name_title').focus();
        swal({
            title: "กรุณาเลือกคำนำหน้าชื่อ",
            text: "",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#26dad2",
            confirmButtonText: 'OK!',
        });
        return false;
    }
    else if ($('#birthday').val() == '') {
        $("#form_user_birthday").addClass("form-group has-danger");
        $("#label_user_birthday").addClass("text-danger");
        $("#birthday").addClass("form-control form-control-danger");
        $('#birthday').focus();
        swal({
            title: "กรุณาเลือกวัน/เดือน/ปีเกิด",
            text: "",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#26dad2",
            confirmButtonText: 'OK!',
        });
        return false;
    }
    else if ($('#user_education').val() == '') {
        $("#form_user_education").addClass("form-group has-danger");
        $("#label_user_education").addClass("text-danger");
        $("#user_education").addClass("form-control form-control-danger");
        $('#user_education').focus();
        swal({
            title: "กรุณาใส่ข้อมูลการศึกษา",
            text: "",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#26dad2",
            confirmButtonText: 'OK!',
        });
        return false;
    } 
    else if ($('#user_tel').val() == '') {
        $("#form_user_tel").addClass("form-group has-danger");
        $("#label_user_tel").addClass("text-danger");
        $("#user_tel").addClass("form-control form-control-danger");
        $('#user_tel').focus();
        swal({
            title: "กรุณาใส่เบอร์โทรศัพท์",
            text: "",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#26dad2",
            confirmButtonText: 'OK!',
        });
        return false;
    } 
    else if ($('#user_email').val() == '') {
        $("#form_user_email").addClass("form-group has-danger");
        $("#label_user_email").addClass("text-danger");
        $("#user_email").addClass("form-control form-control-danger");
        $('#user_email').focus();
        swal({
            title: "กรุณาใส่อีเมล",
            text: "",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#26dad2",
            confirmButtonText: 'OK!',
        });
        return false;
    }
    else if ($('#user_local_address').val() == '') {
        $("#form_user_local_address").addClass("form-group has-danger");
        $("#label_user_local_address").addClass("text-danger");
        $("#user_local_address").addClass("form-control form-control-danger");
        $('#user_local_address').focus();
        swal({
            title: "กรุณาใส่ภูมิลำเนา",
            text: "",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#26dad2",
            confirmButtonText: 'OK!',
        });
        return false;
    } 
    else if ($('#user_local_province').val() == '') {
        $("#form_user_local_province").addClass("form-group has-danger");
        $("#label_user_local_province").addClass("text-danger");
        $("#user_local_province").addClass("form-control form-control-danger");
        $('#user_local_province').focus();
        swal({
            title: "กรุณาเลือกจังหวัด (ภูมิลำเนา)",
            text: "",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#26dad2",
            confirmButtonText: 'OK!',
        });
        return false;
    } 
    else if ($('#user_address').val() == '') {
        $("#form_user_address").addClass("form-group has-danger");
        $("#label_user_address").addClass("text-danger");
        $("#user_address").addClass("form-control form-control-danger");
        $('#user_address').focus();
        swal({
            title: "กรุณาใส่ที่อยู่ปัจจุบัน",
            text: "",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#26dad2",
            confirmButtonText: 'OK!',
        });
        return false;
    }
    else if ($('#user_province').val() == '') {
        $("#form_user_province").addClass("form-group has-danger");
        $("#label_user_province").addClass("text-danger");
        $("#user_province").addClass("form-control form-control-danger");
        $('#user_province').focus();
        swal({
            title: "กรุณาเลือกจังหวัดที่อยู่ปัจจุบัน",
            text: "",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#26dad2",
            confirmButtonText: 'OK!',
        });
        return false;
    } 
    else if ($('#user_id_card').val() == '') {
        $("#form_user_id_card").addClass("form-group has-danger");
        $("#label_user_id_card").addClass("text-danger");
        $("#user_id_card").addClass("form-control form-control-danger");
        $('#user_id_card').focus();
        swal({
            title: "กรุณาใส่หมายเลขบัตรประชาชน",
            text: "",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#26dad2",
            confirmButtonText: 'OK!',
        });
        return false;
    } 
    else if ($('#start_working').val() == '') {
        $("#form_user_start_working").addClass("form-group has-danger");
        $("#label_user_start_working").addClass("text-danger");
        $("#start_working").addClass("form-control form-control-danger");
        $('#start_working').focus();
        swal({
            title: "กรุณาเลือกวันที่บรรจุ",
            text: "",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#26dad2",
            confirmButtonText: 'OK!',
        });
        return false;
    } 
    else if ($('#user_work_type').val() == '') {
        $("#form_user_work_type").addClass("form-group has-danger");
        $("#label_user_work_type").addClass("text-danger");
        $("#user_work_type").addClass("form-control form-control-danger");
        $('#user_work_type').focus();
        swal({
            title: "กรุณาใส่ประเภทสายงาน",
            text: "",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#26dad2",
            confirmButtonText: 'OK!',
        });
        return false;
    } 
    else if ($('#user_budget_type').val() == '') {
        $("#form_user_budget_type").addClass("form-group has-danger");
        $("#label_user_budget_type").addClass("text-danger");
        $("#user_budget_type").addClass("form-control form-control-danger");
        $('#user_budget_type').focus();
        swal({
            title: "กรุณาใส่ประเภทงบประมาณที่จัดจ้าง",
            text: "",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#26dad2",
            confirmButtonText: 'OK!',
        });
        return false;
    } 
    else if ($('#user_type').val() == '') {
        $("#form_user_type").addClass("form-group has-danger");
        $("#label_user_type").addClass("text-danger");
        $("#user_type").addClass("form-control form-control-danger");
        $('#user_type').focus();
        swal({
            title: "กรุณาใส่ประเภทบุคลากร",
            text: "",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#26dad2",
            confirmButtonText: 'OK!',
        });
        return false;
    } 
    else if ($('#user_username').val() == '') {
        $("#form_user_username").addClass("form-group has-danger");
        $("#label_user_username").addClass("text-danger");
        $("#user_username").addClass("form-control form-control-danger");
        $('#user_username').focus();
        swal({
            title: "กรุณาใส่ Username",
            text: "",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#26dad2",
            confirmButtonText: 'OK!',
        });
        return false;
    }
    else if ($('#user_password').val() == '') {
        $("#form_user_password").addClass("form-group has-danger");
        $("#label_user_password").addClass("text-danger");
        $("#user_password").addClass("form-control form-control-danger");
        $('#user_password').focus();
        swal({
            title: "กรุณาใส่รหัสผ่าน",
            text: "",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#26dad2",
            confirmButtonText: 'OK!',
        });
        return false;
    }
    else if ($('#user_cpassword').val() == '') {
        $("#form_user_cpassword").addClass("form-group has-danger");
        $("#label_user_cpassword").addClass("text-danger");
        $("#user_cpassword").addClass("form-control form-control-danger");
        $('#user_cpassword').focus();
        swal({
            title: "กรุณายืนยันรหัสผ่าน",
            text: "",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#26dad2",
            confirmButtonText: 'OK!',
        });
        return false;
    }
    else if ($('#user_password').val() != $('#user_cpassword').val()) {
        $("#form_user_password").addClass("form-group has-danger");
        $("#label_user_password").addClass("text-danger");
        $("#user_password").addClass("form-control form-control-danger");
        $('#user_password').focus();
        $("#form_user_cpassword").addClass("form-group has-danger");
        $("#label_user_cpassword").addClass("text-danger");
        $("#user_cpassword").addClass("form-control form-control-danger");
        $('#user_password').val("");
        $('#user_cpassword').val("");
        swal({
            title: "รหัสผ่านไม่ตรงกัน",
            text: "",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#26dad2",
            confirmButtonText: 'OK!',
        });
        return false;
    }
    else if ($('#user_status').val() == '') {
        $("#form_user_status").addClass("form-group has-danger");
        $("#label_user_status").addClass("text-danger");
        $("#user_status").addClass("form-control form-control-danger");
        $('#user_status').focus();
        swal({
            title: "กรุณาเลือกสถานะ",
            text: "",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#26dad2",
            confirmButtonText: 'OK!',
        });
        return false;
    }
    else {
        swal({
            title: "คุณต้องการบันทึกข้อมูล?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#26dad2",
            confirmButtonText: "บันทึก",
            cancelButtonText: "ยกเลิก",
            closeOnConfirm: false,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                //myDropzone.on("queuecomplete", function () {  //รอให้รูปอัพโหลดเสร็จ ค่อยทำต่อ
                var form_user = $("#form_user");
                $.ajax({
                    url: "php/user_process.php",
                    type: "post", //กำหนดให้มีรูปแบบเป็น post
                    data: form_user.serialize() + "&edit_user=edit_user",
                    success: function (data) {
                        if (data == "yes") {
                            myDropzone.processQueue()   //อัพโหลดรูปเมื่อกดบันทึก
                            swal({
                                title: "บันทึกข้อมูลเรียบร้อย",
                                text: "",
                                type: "success",
                                confirmButtonColor: "#26dad2",
                                confirmButtonText: "OK"
                            }, function () {
                                window.location.replace("index.php");
                            }); // swal
                        } // if data == yes
                        else {
                            console.log(data);
                        }
                    } // success data
                }); // ajax
                //})
            }// isConfirm
            else {
                return false;
            }
        });
    }
}); // #edit button