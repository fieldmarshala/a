<?php

include("inc/connect.php");
include('php/checkuser_login.php');
require 'assets/plugins/phpspreadsheet/vendor/autoload.php';
$db = new database();
$con = $db->connect();
error_reporting(0);

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

$treatise_type = "research";
$today = DateTimeNew(date("Y/m/d"));
$search_name = $_GET['search_name'];
$search_surname = $_GET['search_surname'];
$Getresearch_status = $_GET['research_status'];
if ($search_name) {
	$sql_research = "SELECT author.*,research.* ";
	$sql_research .= "FROM author,research ";
	$sql_research .= "WHERE author.treatise_id = research.item_id ";
	$sql_research .= "and author.name = '$search_name' ";
	$sql_research .= "and author.surname = '$search_surname' ";
	$sql_research .= "and treatise_type = 'research' ";
	if ($Getresearch_status) {
		if ($Getresearch_status == "overdue") {
			$sql_research .= "and (research_status = 'start' or research_status = 'processing') ";
			$sql_research .= "and research_processdate < '$today' ";
		} else {
			$sql_research .= "and research_status = '$Getresearch_status' ";
		}
	}
} else {
	$sql_research = "SELECT * FROM research ";
	if ($Getresearch_status) {
		if ($Getresearch_status == "overdue") {
			$sql_research .= "WHERE (research_status = 'start' or research_status = 'processing') ";
			$sql_research .= "and research_processdate < '$today' ";
		} else {
			$sql_research .= "WHERE research_status = '$Getresearch_status' ";
		}
	}
}
$sql_research .= "ORDER BY research_name";

$query_research = $con->query($sql_research);

$spreadsheet = new Spreadsheet();
$spreadsheet->getDefaultStyle()->getFont()->setName('TH Sarabun New');
$spreadsheet->getDefaultStyle()->getFont()->setSize(18);
$spreadsheet->getDefaultStyle()->getAlignment()->setHorizontal('center');
$spreadsheet->getActiveSheet()->getStyle('A1:J1')->getFont()->setSize(22);
$spreadsheet->getActiveSheet()->getStyle('A1:J1')->getFont()->setBold('Bold');
$spreadsheet->getActiveSheet()->getStyle('J')->getAlignment()->setHorizontal('left');
for ($col = 'A'; $col != 'K'; $col++) {
	$spreadsheet->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
}
$sheet = $spreadsheet->getActiveSheet();
$sheet->setCellValue('A1', 'เรื่องที่');
$sheet->setCellValue('B1', 'ชื่องานวิจัย');
$sheet->setCellValue('C1', 'สถานะงานวิจัย');
$sheet->setCellValue('D1', 'วันที่เริ่มโครงร่างวิจัย');
$sheet->setCellValue('E1', 'วันที่งานวิจัยเสร็จสิ้น');
$sheet->setCellValue('F1', 'ระยะเวลาดำเนินการ');
$sheet->setCellValue('G1', 'แหล่งเงินทุนของงานวิจัย');
$sheet->setCellValue('H1', 'ปีงบประมาณที่ขอทุนวิจัย');
$sheet->setCellValue('I1', 'จำนวนงบประมาณที่ขอทุนวิจัย');
$sheet->setCellValue('J1', 'รายชื่อผู้แต่ง');

if ($query_research->num_rows > 0) {
	$result_row = 1;
	$i = 1;
	while ($result_research = $query_research->fetch_object()) {

		$item_id = $result_research->item_id;
		//fetch author
		$sql_author = "SELECT * FROM author WHERE treatise_type = '$treatise_type' AND treatise_id = '$item_id' ORDER BY author_id";
		$query_author = $con->query($sql_author);
		$num_author = $query_author->num_rows;

		$research_name = $result_research->research_name;
		$research_status = $result_research->research_status;
		$research_budget = $result_research->research_budget;
		$research_startdate = DateConvertBase($result_research->research_startdate);
		$research_enddate = DateConvertBase($result_research->research_enddate);
		$research_processdate = DateConvertBase($result_research->research_processdate);
		$research_budget_from = $result_research->research_budget_from;
		$research_budget_year = $result_research->research_budget_year;

		if ($research_status == "complete") {
			$research_status_show = "เสร็จสิ้น";
		} else if ($research_status == "processing") {
			$research_status_show = "กำลังดำเนินการ";
		} else if ($research_status == "start") {
			$research_status_show = "เริ่มโครงร่างวิจัย";
		} else if ($research_status == "overdue") {
			$research_status_show = "เกินกำหนดระยะเวลา";
		}

		$rowNum = $result_row + 1;

		$sheet->setCellValue('A' . $rowNum, $i);
		$sheet->setCellValue('B' . $rowNum, $research_name);
		$sheet->setCellValue('C' . $rowNum, $research_status_show);
		$sheet->setCellValue('D' . $rowNum, $research_startdate);
		$sheet->setCellValue('E' . $rowNum, $research_enddate);
		$sheet->setCellValue('F' . $rowNum, $research_processdate);
		$sheet->setCellValue('G' . $rowNum, $research_budget_from);
		$sheet->setCellValue('H' . $rowNum, $research_budget_year);
		$sheet->setCellValue('I' . $rowNum, $research_budget);
		$sheet->setCellValue('J' . $rowNum, "จำนวนผู้แต่ง " . $num_author . " คน");

		$ii = 1;
		while ($result_author = $query_author->fetch_object()) {
			$author_name = $result_author->name;
			$author_surname = $result_author->surname;
			$author_name_title = $result_author->name_title;
			$author_academic_rank = $result_author->academic_rank;
			if ($author_name_title == "dr") {
				$name_title_show = "ดร.";
			} else {
				$name_title_show = "";
			}

			if ($author_academic_rank == "pro") {
				$academic_rank_show = "ศ.";
			} else if ($author_academic_rank == "asso") {
				$academic_rank_show = "ร.ศ.";
			} else if ($author_academic_rank == "assis") {
				$academic_rank_show = "ผ.ศ.";
			} else if ($author_academic_rank == "lec") {
				if ($name_title_show == "ดร.") {
					$academic_rank_show = "อาจารย์ ";
				} else {
					$academic_rank_show = "อาจารย์ ";
				}
			}
			$author_row = $result_row + 2;
			$sheet->setCellValue('J' . $author_row, "คนที่ " . $ii . " : " . $academic_rank_show . $name_title_show . " " . $author_name . " " . $author_surname);
			$result_row++;
			$ii++;
		}
		$result_row++;
		$i++;
	}
}

$filename = 'งานวิจัย-' . time() . '.xlsx';
// Redirect output to a client's web browser (Xlsx)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.

$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
