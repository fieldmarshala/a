<?php

include("inc/connect.php");
include('php/checkuser_login.php');
require 'assets/plugins/phpspreadsheet/vendor/autoload.php';
$db = new database();
$con = $db->connect();
error_reporting(0);

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

$treatise_type = "research_article";
$search_name = $_GET['search_name'];
$search_surname = $_GET['search_surname'];
$start_year = $_GET['start_year'];
$end_year = $_GET['end_year'];

if ($search_name) {
    $sql_research_article = "SELECT author.*,research_article.* ";
    $sql_research_article .= "FROM author,research_article ";
    $sql_research_article .= "WHERE author.treatise_id = research_article.item_id ";
    $sql_research_article .= "and author.name = '$search_name' ";
    $sql_research_article .= "and author.surname = '$search_surname' ";
    $sql_research_article .= "and treatise_type = 'research_article' ";
    if ($start_year) {
        $sql_research_article .= "and year_make >= '$start_year' ";
    } else if ($end_year) {
        $sql_research_article .= "and year_make <= '$end_year' ";
    } else if ($start_year && $end_year) {
        $sql_research_article .= "and year_make >= '$start_year' and year_make <= '$end_year' ";
    }
} else {
    $sql_research_article = "SELECT * FROM research_article ";
    if ($start_year) {
        $sql_research_article .= "WHERE year_make >= '$start_year' ";
    } else if ($end_year) {
        $sql_research_article .= "WHERE year_make <= '$end_year' ";
    } else if ($start_year && $end_year) {
        $sql_research_article .= "WHERE year_make >= '$start_year' and year_make <= '$end_year' ";
    }
}

$sql_research_article .= "ORDER BY research_article_name";

$query_research_article = $con->query($sql_research_article);

$spreadsheet = new Spreadsheet();
$spreadsheet->getDefaultStyle()->getFont()->setName('TH Sarabun New');
$spreadsheet->getDefaultStyle()->getFont()->setSize(18);
$spreadsheet->getDefaultStyle()->getAlignment()->setHorizontal('center');
$spreadsheet->getActiveSheet()->getStyle('A1:K1')->getFont()->setSize(22);
$spreadsheet->getActiveSheet()->getStyle('A1:K1')->getFont()->setBold('Bold');
$spreadsheet->getActiveSheet()->getStyle('K')->getAlignment()->setHorizontal('left');
for ($col = 'A'; $col != 'L'; $col++) {
    $spreadsheet->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
}
$sheet = $spreadsheet->getActiveSheet();
$sheet->setCellValue('A1', 'เรื่องที่');
$sheet->setCellValue('B1', 'ชื่อบทความวิจัย');
$sheet->setCellValue('C1', 'จำนวนหน้า');
$sheet->setCellValue('D1', 'ปีที่แต่ง');
$sheet->setCellValue('E1', 'ชื่อวารสาร');
$sheet->setCellValue('F1', 'ปีที่');
$sheet->setCellValue('G1', 'ฉบับที่');
$sheet->setCellValue('H1', 'จากเดือน');
$sheet->setCellValue('I1', 'ถึงเดือน');
$sheet->setCellValue('J1', 'ISSN');
$sheet->setCellValue('K1', 'รายชื่อผู้แต่ง');

if ($query_research_article->num_rows > 0) {
    $result_row = 1;
    $i = 1;
    while ($result_research_article = $query_research_article->fetch_object()) {

        $item_id = $result_research_article->item_id;
        //fetch author
        $sql_author = "SELECT * FROM author WHERE treatise_type = '$treatise_type' AND treatise_id = '$item_id' ORDER BY author_id";
        $query_author = $con->query($sql_author);
        $num_author = $query_author->num_rows;

        $research_article_name = $result_research_article->research_article_name;
        $year_make = $result_research_article->year_make;
        $page = $result_research_article->page;
        $year = $result_research_article->year;
        $journal_name = $result_research_article->journal_name;
        $copy_no = $result_research_article->copy_no;
        $start_month = $result_research_article->start_month;
        $endmonth = $result_research_article->end_month;
        $issn = $result_research_article->issn;

        $rowNum = $result_row + 1;

        $sheet->setCellValue('A' . $rowNum, $i);
        $sheet->setCellValue('B' . $rowNum, $research_article_name);
        $sheet->setCellValue('C' . $rowNum, $page);
        $sheet->setCellValue('D' . $rowNum, $year_make);
        $sheet->setCellValue('E' . $rowNum, $journal_name);
        $sheet->setCellValue('F' . $rowNum, $year);
        $sheet->setCellValue('G' . $rowNum, $copy_no);
        $sheet->setCellValue('H' . $rowNum, $start_month);
        $sheet->setCellValue('I' . $rowNum, $endmonth);
        $sheet->setCellValue('J' . $rowNum, $issn);
        $sheet->setCellValue('K' . $rowNum, "จำนวนผู้แต่ง " . $num_author . " คน");

        $ii = 1;
        while ($result_author = $query_author->fetch_object()) {
            $author_name = $result_author->name;
            $author_surname = $result_author->surname;
            $author_name_title = $result_author->name_title;
            $author_academic_rank = $result_author->academic_rank;
            if ($author_name_title == "dr") {
                $name_title_show = "ดร.";
            } else {
                $name_title_show = "";
            }

            if ($author_academic_rank == "pro") {
                $academic_rank_show = "ศ.";
            } else if ($author_academic_rank == "asso") {
                $academic_rank_show = "ร.ศ.";
            } else if ($author_academic_rank == "assis") {
                $academic_rank_show = "ผ.ศ.";
            } else if ($author_academic_rank == "lec") {
                if ($name_title_show == "ดร.") {
                    $academic_rank_show = "อาจารย์ ";
                } else {
                    $academic_rank_show = "อาจารย์ ";
                }
            }
            $author_row = $result_row + 2;
            $sheet->setCellValue('K' . $author_row, "คนที่ " . $ii . " : " . $academic_rank_show . $name_title_show . " " . $author_name . " " . $author_surname);
            $result_row++;
            $ii++;
        }
        $result_row++;
        $i++;
    }
}

$filename = 'บทความวิจัย-' . time() . '.xlsx';
// Redirect output to a client's web browser (Xlsx)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.

$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
